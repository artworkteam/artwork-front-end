module.exports = {
    "parser": "babel-eslint",
    "extends": "airbnb",
    "plugins": [
        "react",
        "jsx-a11y",
        "import"
    ],
    "env": {
      "browser": true,
    },
    "rules": {
      "semi": 1,
      "strict": 0,
      "jsx-quotes": [1, "prefer-single"],
      "react/jsx-filename-extension": [1, { "extensions": [".js", ".jsx"] }],
      "react/jsx-first-prop-new-line": 0,
      "react/require-default-props": 0,
      "react/sort-comp": 0,
      "spaced-comment": 0,
      "jsx-a11y/no-static-element-interactions": 0,
      "jsx-a11y/href-no-hash": 1,
      "import/prefer-default-export": 0,
      "import/no-extraneous-dependencies": [2, { "devDependencies": true }],
      "no-empty": [2, { "allowEmptyCatch": true }],
    },
};
