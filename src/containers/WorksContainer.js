import React from 'react';
import { connect } from 'react-redux';
import { Works } from '../components/';

const WorkContainer = props => (
  <Works {...props} />
);

const mapStateToProps = (state, ownProps) => {
  const { sort: filter, author: byUser } = ownProps.location.query;
  const { users, works } = state.entities;
  const { auth } = state;
  const { loading, items, nextPage, hasMore, error } = state.works;

  return {
    filter,
    byUser,
    auth,
    users,
    works,
    loading,
    worksSelected: items,
    nextPage,
    hasMore,
    error,
  };
};

export default connect(mapStateToProps)(WorkContainer);
