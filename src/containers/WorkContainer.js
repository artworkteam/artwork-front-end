import React from 'react';
import { connect } from 'react-redux';
import { Work } from '../components/';

const WorkContainer = props => (
  <Work {...props} />
);

const mapStateToProps = (store, ownProps) => {
  const { user } = store.auth;
  const { workId } = ownProps.params;
  const { author: selectedAuthor, comment: selectedComment } = ownProps.location.query;
  const { loading } = store.works;
  const { loading: commentsLoading, items: commentsIds } = store.comments;
  const { works, users, comments } = store.entities;
  return {
    me: user,
    workId,
    loading,
    work: works[workId],
    commentsLoading,
    comments: commentsIds.map(id => comments[id]),
    users,
    selectedAuthor,
    selectedComment,
  };
};

export default connect(mapStateToProps)(WorkContainer);
