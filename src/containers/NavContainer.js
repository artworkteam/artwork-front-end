import React from 'react';
import { connect } from 'react-redux';
import { Nav } from '../components/';

const NavContainer = props => (
  <Nav {...props} />
);

const mapStateToProps = (state, ownProps) => {
  const { sort, author: byUser } = ownProps.location.query;
  const { pathname } = ownProps.location;
  const { loading: userLoading, user } = state.auth;

  const isMainPage = pathname === '/' || pathname === '/works';
  const isMineWorksPage = byUser === (user && user.uuid);
  const isWorksByUserPage = !!byUser;

  return {
    filter: sort,
    isMainPage,
    isMineWorksPage,
    isWorksByUserPage,
    user,
    userLoading,
  };
};

export default connect(mapStateToProps)(NavContainer);
