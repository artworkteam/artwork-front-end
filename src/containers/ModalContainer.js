import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Modal } from '../components/';

const ModalContainer = (props) => {
  if (!props.modalType) {
    return null;
  }

  return <Modal {...props} />;
};

ModalContainer.propTypes = {
  modalType: PropTypes.string,
};

ModalContainer.defaultProps = {
  modalType: null,
};

function mapStateToProps(state) {
  const { modal } = state;

  return modal;
}

export default connect(mapStateToProps)(ModalContainer);
