import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import _ from 'lodash';
import {
  fetchNotifications,
  massReadNotifications,
  clearNotifications,
} from '../actions/ServerNotificationsActions';
import NotificationsTimeline from '../components/NotificationsTimeline/NotificationsTimeline';

@connect((store) => {
  const { hasMore, loading, nextPage, items } = store.serverNotifications;
  const { notifications } = store.entities;
  return {
    notifications: notifications || {},
    items,
    loading,
    nextPage,
    hasMore,
  };
}, {
  fetchNotifications,
  massReadNotifications,
  clearNotifications,
})
export default class ServerNotificationsContainer extends Component {
  static propTypes = {
    items: PropTypes.arrayOf(PropTypes.string).isRequired,
    loading: PropTypes.bool,
    nextPage: PropTypes.number.isRequired,
    hasMore: PropTypes.bool,

    notifications: PropTypes.shape({}),

    fetchNotifications: PropTypes.func.isRequired,
    massReadNotifications: PropTypes.func.isRequired,
    clearNotifications: PropTypes.func.isRequired,
  };

  componentWillMount() {
    this.props.clearNotifications();
  }

  componentWillReceiveProps(nextProps) {
    const { notifications, items } = nextProps;
    if (Array.isArray(items) && this.props.items !== items) {
      const toRead = [];
      items.forEach((id) => {
        const n = notifications[id];
        if (!n.read) toRead.push(id);
      });
      if (toRead.length > 0) this.props.massReadNotifications(toRead);
    }
  }

  loadMore = () => this.props.fetchNotifications({ page: this.props.nextPage });

  render() {
    const { items, loading, hasMore, nextPage, notifications } = this.props;
    return (
      <NotificationsTimeline
        loading={loading}
        hasMore={hasMore}
        nextPage={nextPage}
        loadMore={this.loadMore}
        notifications={_.map(items, o => notifications[o])}
      />
    );
  }
}
