import React, { Component } from 'react';
import { connect } from 'react-redux';
import { clearAlert } from '../actions/AlertsActions';
import { Alerts } from '../components/';

@connect((store) => {
  const { message, type } = store.alerts;
  return {
    message,
    type,
  };
}, {
  clearAlert,
})
export default class AlertsContainer extends Component {
  render() {
    return <Alerts {...this.props} />;
  }
}
