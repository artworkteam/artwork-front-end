import React from 'react';
import { connect } from 'react-redux';
import { Suggestions } from '../components/';

const SuggestionsContainer = props => (
  <Suggestions {...props} />
);

const mapStateToProps = (state, ownProps) => {
  const { user: me } = state.auth;
  const { workId, add: addParam } = ownProps.params;
  const { author, suggestion: querySuggestion } = ownProps.location.query;
  const { works } = state.entities;
  const { auth } = state;
  const { suggestionsByAuthor, selectedSuggestion, previewSuggestion, initial } = state.suggestions;
  const isAddSuggestionMode = addParam === 'add';

  return {
    me,
    workId,
    suggestionsByAuthor,
    authorQuery: author,
    initial,
    selectedSuggestion,
    querySuggestion,
    previewSuggestion,
    auth,
    works,
    isAddSuggestionMode,
  };
};

export default connect(mapStateToProps)(SuggestionsContainer);
