import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { initAuth } from '../actions/AuthActions';
import { Footer, Content } from '../components/';
import NavContainer from '../containers/NavContainer';
import ModalContainer from '../containers/ModalContainer';
import AlertsContainer from './AlertsContainer';

class AppContainer extends Component {
  static propTypes = {
    dispatch: PropTypes.func.isRequired,
    location: PropTypes.shape({}).isRequired,
    children: PropTypes.node.isRequired,
  };

  componentDidMount() {
    const { dispatch } = this.props;

    dispatch(initAuth());
  }

  render() {
    const { location, params, children } = this.props;

    return (
      <div className="app">
        <AlertsContainer />
        <NavContainer location={location} />
        <Content>
          { children }
        </Content>
        <Footer />
        <ModalContainer location={location} params={params} />
      </div>
    );
  }
}

export default connect(null)(AppContainer);
