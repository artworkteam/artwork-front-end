import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import {
  openNotificationsSse,
  closeNotificationsSse,
  fetchUnreadNotifications,
  readNotification,
} from '../actions/ServerNotificationsActions';
import {
  getUnread,
  hasNew,
} from '../reducers/server-notifications';
import { push } from '../actions/RouteActions';

import NotificationButton from '../components/NotificationButton/NotificationButton';

@connect((store, ownProps) => {
  const state = store.serverNotifications;
  const { onListHidden } = ownProps;
  const { notifications } = store.entities;
  return {
    notifications,
    items: getUnread(state),
    unread: hasNew(state),
    onListHidden,
  };
}, {
  openNotificationsSse,
  closeNotificationsSse,
  fetchUnreadNotifications,
  readNotification,
  push,
})
export default class SseNotificationsContainer extends Component {
  static propTypes = {
    notifications: PropTypes.object.isRequired,
    items: PropTypes.arrayOf(PropTypes.string).isRequired,
    unread: PropTypes.bool.isRequired,
    onListHidden: PropTypes.func.isRequired,

    openNotificationsSse: PropTypes.func.isRequired,
    closeNotificationsSse: PropTypes.func.isRequired,
    fetchUnreadNotifications: PropTypes.func.isRequired,
    readNotification: PropTypes.func.isRequired,
    push: PropTypes.func.isRequired,
  };

  componentWillMount() {
    this.props.fetchUnreadNotifications();
    this.props.openNotificationsSse();
  }

  componentWillUnmount() {
    this.props.closeNotificationsSse();
  }

  linkToNotificationTrigger = (notification) => {
    const { type, workId, commentId, suggestionId, initiator } = notification;

    switch (type) {
      case 'WORK_COMMENTED':
        return `/works/${workId}?comment=${commentId}`;
      case 'SUGGESTION_CREATED':
        return `/works/${workId}/suggestions/?author=${initiator.uuid}&suggestion=${suggestionId}`;
      case 'WORK_LIKED':
        return `/works/${workId}`;
      case 'COMMENT_LIKED':
        return `/works/${workId}?comment=${commentId}`;
      default:
        return `/works/${workId}`;
    }
  }

  onShowAll = () => this.props.push('/notifications');
  onShowNotification = (id) => {
    const { notifications } = this.props;
    const notification = notifications[id];

    this.props.push(this.linkToNotificationTrigger(notification));
    this.props.readNotification(id);
  };

  render() {
    const { unread, notifications, items, onListHidden } = this.props;
    return (
      <NotificationButton
        unread={unread}
        notifications={notifications}
        items={items}
        onShowAll={this.onShowAll}
        onShowNotification={this.onShowNotification}
        onListHidden={onListHidden}
      />
    );
  }
}
