import { createStore, applyMiddleware } from 'redux';
import thunkMiddleware from 'redux-thunk';
import LogRocket from 'logrocket';
import sseMiddleware from '../sse/middleware';
import { composeWithDevTools } from 'redux-devtools-extension/developmentOnly';
import * as actionCreators from '../actions/';
import rootReducer from '../reducers';

const composeEnhancers = composeWithDevTools({
  actionCreators,
});

export default function configureStore(initialState) {
  const store = createStore(
    rootReducer,
    initialState,
    composeEnhancers(
      process.env.NODE_ENV === 'production'
      ? applyMiddleware(thunkMiddleware, sseMiddleware, LogRocket.reduxMiddleware())
      : applyMiddleware(thunkMiddleware, sseMiddleware),
    ),
  );
  return store;
}
