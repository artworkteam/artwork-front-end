import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { pinWorkById, likeWork, updateWorkDescription } from '../../actions/WorksActions';
import { getComments, postComment, likeComment } from '../../actions/CommentsActions';
import { WorkDesc, WorkInfo, BackButton } from '../../components/';
import LoaderSpinner from '../../components/LoaderSpinner/LoaderSpinner';
import styles from './work.css';

export default class Work extends Component {
  static propTypes = {
    dispatch: PropTypes.func.isRequired,
    me: PropTypes.shape({}),
    workId: PropTypes.string.isRequired,
    loading: PropTypes.bool.isRequired,
    work: PropTypes.shape({}),
    commentsLoading: PropTypes.bool.isRequired,
    comments: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
    users: PropTypes.objectOf(PropTypes.shape({})).isRequired,
    selectedComment: PropTypes.string,
  };

  componentWillMount() {
    const { workId, dispatch } = this.props;
    dispatch(pinWorkById(workId));
    dispatch(getComments(workId));
  }

  onLike = () => {
    const { dispatch, workId, work } = this.props;
    if (work) dispatch(likeWork(workId, !work.alreadyLiked));
  };

  onCommentSubmit = (data, dispatch) => dispatch(postComment(this.props.workId, data));
  onCommentLike = ({ commentId, alreadyLiked }) =>
    this.props.dispatch(likeComment(this.props.workId, commentId, !alreadyLiked));

  onWorkDescriptionSubmit = (data, dispatch) =>
    dispatch(updateWorkDescription(this.props.workId, data));


  render() {
    const { me, loading, work,
      commentsLoading, comments,
      users, dispatch, selectedComment,
    } = this.props;

    if (loading) return (<div className={styles.spinner}><LoaderSpinner /></div>);
    if (!work) return <div className={styles.work} />; // Probably error

    const owner = users[work.owner];

    return (
      <div className={styles.work}>
        <BackButton dispatch={dispatch} />
        <div className={styles.wrapper}>
          <div className={styles.workDesc}>
            <WorkDesc
              work={work}
              onLike={me ? this.onLike : null}
              commentsLoading={commentsLoading}
              comments={comments}
              selectedComment={selectedComment}
              users={users}
              onCommentSubmit={me ? this.onCommentSubmit : null}
              onCommentLike={me ? this.onCommentLike : null}
              dispatch={dispatch}
            />
          </div>
          <div className={styles.workInfo}>
            <WorkInfo
              work={work}
              owner={owner}
              me={me}
              onWorkDescriptionSubmit={this.onWorkDescriptionSubmit}
              dispatch={dispatch}
            />
          </div>
        </div>
      </div>
    );
  }
}
