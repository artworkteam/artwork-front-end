import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Field, reduxForm } from 'redux-form';
import { Form } from '../../components';
import { clearComments, getComments } from '../../actions/CommentsActions';
import { addSuggestion, getSuggestions, resetSelected } from '../../actions/SuggestionsActions';
import { push } from '../../actions/RouteActions';
import styles from './suggestionCommentForm.css';

class SuggestionCommentForm extends Component {
  static propTypes = {
    dispatch: PropTypes.func.isRequired,
    handleSubmit: PropTypes.func.isRequired,
    workId: PropTypes.string.isRequired,
    x: PropTypes.number.isRequired,
    y: PropTypes.number.isRequired,
  }

  componentDidMount() {
    setTimeout(() => this.textRef.getRenderedComponent().focus(), 10);
  }

  formSubmit = ({ text }, dispatch) => {
    const { workId, x, y } = this.props;

    return dispatch(addSuggestion(workId, x, y, text))
    .then(() => dispatch(getSuggestions(workId)))
    .then(() => dispatch(resetSelected()))
    .then(() => dispatch(push(`/works/${workId}/suggestions`)))
    .then(() => dispatch(clearComments()))
    .then(() => dispatch(getComments(workId)));
  }

  render() {
    return (
      <Form className={styles.form} {...this.props} onSubmit={this.formSubmit}>
        <Field
          className={styles.comment}
          name='text'
          placeholder='Напишите комментарий'
          component='textarea'
          ref={(ref) => { this.textRef = ref; }}
          withRef
        />
        <button type='submit' className={styles.submit}>Добавить комментарий</button>
      </Form>
    );
  }
}

export default reduxForm({
  form: 'suggestionComment',
})(SuggestionCommentForm);
