import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { deleteWork, clearWorks } from '../../actions/WorksActions';
import { showSuccessAlert } from '../../actions/AlertsActions';
import { hideModal } from '../../actions/ModalActions';
import { push } from '../../actions/RouteActions';
import { ModalCloseButton, DeleteWorkForm } from '../../components';
import styles from './modalDeleteWork.css';


export default class ModalDeleteWork extends Component {
  static propTypes = {
    closeModal: PropTypes.func.isRequired,
    params: PropTypes.shape({
      workId: PropTypes.string,
    }).isRequired,
  };

  onSubmit = (data, dispatch) => {
    const { workId } = this.props.params;

    return dispatch(deleteWork(workId));
  }

  onSubmitSuccess = (data, dispatch) => {
    dispatch(showSuccessAlert('Работа успешно удалена.'));
    dispatch(hideModal());
    dispatch(push('/'));
    dispatch(clearWorks());
  }


  render() {
    const { closeModal } = this.props;

    return (
      <div className={styles.modalDeleteWork}>
        <div className={styles.closeButton}>
          <ModalCloseButton closeModal={closeModal} />
        </div>
        <div className={styles.content}>
          <p className={styles.title}>
            Вы действительно хотите удалить эту работу <br /> и все комментарии к ней?
          </p>
          <hr className={styles.separator} />
          <DeleteWorkForm
            onSubmit={this.onSubmit}
            onSubmitSuccess={this.onSubmitSuccess}
            closeModal={closeModal}
          />
        </div>
      </div>
    );
  }
}
