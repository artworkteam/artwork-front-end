import React from 'react';
import PropTypes from 'prop-types';
import { AuthButton, ModalCloseButton } from '../../components';
import styles from './modalLogin.css';

const ModalLogin = ({ dispatch, closeModal }) => (
  <div className={styles.modalLogin}>
    <div className={styles.closeButton}>
      <ModalCloseButton closeModal={closeModal} />
    </div>
    <div className={styles.content}>
      <p className={styles.title}>Вы можете авторизоваться одним из нескольких способов:</p>
      <hr className={styles.separator} />
      <div className={styles.buttons}>
        <AuthButton className={styles.button} name="vkontakte" dispatch={dispatch} />
        <AuthButton className={styles.button} name="facebook" dispatch={dispatch} />
      </div>
    </div>
  </div>
);

ModalLogin.propTypes = {
  dispatch: PropTypes.func.isRequired,
  closeModal: PropTypes.func.isRequired,
};

export default ModalLogin;
