import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router';
import { WorkCounters, WorkPanel, WorkCommentsList, Button, WorkImage } from '../../components/';
import LoaderSpinner from '../LoaderSpinner/LoaderSpinner';
import styles from './workDesc.css';

const WorkDesc = ({ work, onLike, commentsLoading, comments,
  users, onCommentSubmit, onCommentLike, dispatch, selectedComment }) => (
    <div className={styles.workDesc}>
      <div className={styles.imageContainer}>
        <WorkImage
          imageId={work.imageId}
          type='previewMedium'
          className={styles.image}
        />
        <div className={styles.overlay}>
          <Link to={`/works/${work.workId}/suggestions`}>
            <Button styleType='basicWhiteTransparent'>Посмотреть все маркеры</Button>
          </Link>
        </div>
      </div>
      <div className={styles.counters}>
        <WorkCounters
          work={work}
          onLike={onLike}
          withSuggesitons
        />
      </div>
      {onCommentSubmit &&
        <WorkPanel
          onCommentSubmit={onCommentSubmit}
          workId={work.workId}
          dispatch={dispatch}
        />
      }
      {commentsLoading ?
        <div className={styles.loader}>
          <LoaderSpinner />
        </div> :
        <WorkCommentsList
          workId={work.workId}
          comments={comments}
          selectedComment={selectedComment}
          users={users}
          onCommentLike={onCommentLike}
        />
      }
    </div>
);

WorkDesc.propTypes = {
  work: PropTypes.shape({}).isRequired,
  onLike: PropTypes.func,
  onCommentSubmit: PropTypes.func,
  onCommentLike: PropTypes.func,
  commentsLoading: PropTypes.bool.isRequired,
  comments: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  selectedComment: PropTypes.string,
  users: PropTypes.objectOf(PropTypes.shape({})).isRequired,
  dispatch: PropTypes.func.isRequired,
};

export default WorkDesc;
