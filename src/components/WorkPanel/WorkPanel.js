import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router';
import { Button, WorkCommentForm } from '../../components/';
import { push } from '../../actions/RouteActions';
import styles from './workPanel.css';

export default class WorkPanel extends Component {
  static propTypes = {
    onCommentSubmit: PropTypes.func.isRequired,
    workId: PropTypes.string.isRequired,
    dispatch: PropTypes.func.isRequired,
  };

  state = {
    expanded: false,
  };

  makeCommentClick = (e) => {
    e.preventDefault();
    this.setState({
      expanded: true,
    });
  };

  makeSuggestionClick = (e) => {
    e.preventDefault();

    const { workId, dispatch } = this.props;

    dispatch(push(`/works/${workId}/suggestions/add`));
  };

  closeForm = () => {
    this.setState({
      expanded: false,
    });
    this.commentForm.reset();
  };

  render() {
    const { onCommentSubmit } = this.props;
    const { expanded } = this.state;

    return (
      <div className={styles.workPanel}>
        <div className={styles.buttons}>
          <Button
            className={styles.makeCommentButton}
            styleType='basic'
            onClick={this.makeCommentClick}
            disabled={expanded}
          >
            Оставить комментарий
          </Button>
          <Button
            className={styles.makeSuggestionButton}
            styleType='basicGreen'
            onClick={this.makeSuggestionClick}
            disabled={expanded}
          >
            Указать на работе
          </Button>
        </div>
        <div className={expanded ? styles.formExpanded : styles.formCollapsed}>
          <WorkCommentForm
            ref={ref => (this.commentForm = ref)}
            closeForm={this.closeForm}
            onSubmitSuccess={this.closeForm}
            onSubmit={onCommentSubmit}
          />
        </div>

      </div>
    );
  }
}
