import React from 'react';
import PropTypes from 'prop-types';
import logoImg from './logo.svg';

const Logo = () => (
  <img src={logoImg} alt='' />
);

export default Logo;
