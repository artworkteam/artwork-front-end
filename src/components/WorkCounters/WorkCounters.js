import React from 'react';
import PropTypes from 'prop-types';
import { LikeButton } from '../../components/';
import { ViewIcon, ChatIcon, PlaceholderIcon } from '../Icons/Icons';
import styles from './workCounters.css';

const WorkCounters = ({ work, onLike, withSuggesitons }) => (
  <div className={styles.workCounters}>
    <div className={styles.counter}>
      <div className={styles.iconView}>
        <ViewIcon />
      </div>
      {work.viewsCount}
    </div>
    <div className={styles.counter}>
      <LikeButton
        likesCount={work.likesCount}
        alreadyLiked={work.alreadyLiked}
        onLike={onLike}
      />
    </div>
    <div className={styles.counter}>
      <div className={styles.iconComments}>
        <ChatIcon />
      </div>
      {work.commentsCount}
    </div>
    { withSuggesitons &&
      <div className={styles.counter}>
        <div className={styles.iconSuggestions}>
          <PlaceholderIcon />
        </div>
        {work.suggestionsCount}
      </div>
    }
  </div>
);

WorkCounters.propTypes = {
  work: PropTypes.shape({
    likesCount: PropTypes.number,
    alreadyLiked: PropTypes.bool,
    commentsCount: PropTypes.number,
  }).isRequired,
  onLike: PropTypes.func,
  withSuggesitons: PropTypes.bool,
};

export default WorkCounters;
