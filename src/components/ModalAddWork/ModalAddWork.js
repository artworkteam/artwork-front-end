import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { ModalCloseButton, AddWorkForm } from '../../components';
import { showSuccessAlert } from '../../actions/AlertsActions';
import { addWork, clearWorks } from '../../actions/WorksActions';
import { hideModal } from '../../actions/ModalActions';
import { handleSubmitError } from '../../utils/forms';
import styles from './modalAddWork.css';

export default class ModalAddWork extends Component {
  static propTypes = {
    closeModal: PropTypes.func.isRequired,
  };

  onSubmit = (data, dispatch) => dispatch(addWork(data));

  onSubmitSuccess = (data, dispatch) => {
    dispatch(showSuccessAlert('Поздравляем! Ваша работа успешно загружена и доступна в списке работ.'));
    dispatch(hideModal());
    dispatch(clearWorks());
  };

  render() {
    const { closeModal } = this.props;
    return (
      <div className={styles.modalAddWork}>
        <div />
        <div className={styles.closeButton}>
          <ModalCloseButton closeModal={closeModal} />
        </div>
        <div className={styles.content}>
          <AddWorkForm
            onSubmit={this.onSubmit}
            onSubmitSuccess={this.onSubmitSuccess}
          />
        </div>
      </div>
    );
  }
}
