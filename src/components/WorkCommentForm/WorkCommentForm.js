import React from 'react';
import PropTypes from 'prop-types';
import { reduxForm } from 'redux-form';
import validation, { valid, notEmpty } from '../../utils/validation';
import { Button } from '../../components/';
import TextAreaField from '../TextAreaField/TextAreaField';
import ErrorField from '../ErrorField/ErrorField';
import Form from '../Form/Form';
import styles from './workCommentForm.css';

const WorkCommentForm = ({ closeForm, ...props }) => {
  const closeClick = (e) => {
    e.preventDefault();
    closeForm();
  };

  return (
    <Form className={styles.workCommentForm} {...props} >
      <div className={styles.formWrapper}>
        <TextAreaField
          className={styles.comment}
          name='text'
          minRows={4}
          maxRows={4}
        />
        <ErrorField name='text' />
      </div>
      <div className={styles.buttons}>
        <Button type='submit' styleType='basic'>
          Отправить
        </Button>
        <Button styleType='basicTransparent' onClick={closeClick} >
          Отмена
        </Button>
      </div>
    </Form>
  );
};

WorkCommentForm.propTypes = {
  closeForm: PropTypes.func.isRequired,
};

export default reduxForm({
  form: 'commentForm',
  validate: validation({
    text: valid(notEmpty()),
  }),
})(WorkCommentForm);
