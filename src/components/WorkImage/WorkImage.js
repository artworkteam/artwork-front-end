import React from 'react';
import PropTypes from 'prop-types';
import { CDN_HOSTNAME } from '../../constants/Uploadcare';

const WorkImage = ({ imageId, type, className, alt, onLoad, onError }) => {
  switch (type) {
    case 'previewSmall':
      return (
        <picture>
          <source srcSet={`https://${CDN_HOSTNAME}/${imageId}/-/format/webp/-/quality/best/-/setfill/fafafa/-/scale_crop/1004x941/center/-/sharp/5/,
                           https://${CDN_HOSTNAME}/${imageId}/-/format/webp/-/quality/normal/-/stretch/off/-/setfill/fafafa/-/scale_crop/1434x1344/center/ 2x,
                           https://${CDN_HOSTNAME}/${imageId}/-/format/webp/-/quality/lighter/-/stretch/off/-/setfill/fafafa/-/scale_crop/2868x2688/center/ 4x`}
            media='(max-width: 767px)'
            type='image/webp'
          />
          <source srcSet={`https://${CDN_HOSTNAME}/${imageId}/-/format/auto/-/quality/best/-/setfill/fafafa/-/scale_crop/1004x941/center/-/sharp/5/,
                           https://${CDN_HOSTNAME}/${imageId}/-/format/auto/-/quality/normal/-/stretch/off/-/setfill/fafafa/-/scale_crop/1434x1344/center/ 2x,
                           https://${CDN_HOSTNAME}/${imageId}/-/format/auto/-/quality/lighter/-/stretch/off/-/setfill/fafafa/-/scale_crop/2868x2688/center/ 4x`}
            media='(max-width: 767px)'
          />
          <source srcSet={`https://${CDN_HOSTNAME}/${imageId}/-/format/webp/-/quality/best/-/scale_crop/480x417/center/-/sharp/5/,
                           https://${CDN_HOSTNAME}/${imageId}/-/format/webp/-/quality/normal/-/scale_crop/686x596/center/ 2x,
                           https://${CDN_HOSTNAME}/${imageId}/-/format/webp/-/quality/lighter/-/stretch/off/-/scale_crop/1372x1192/center/ 4x`}
            media='(max-width: 991px)'
            type='image/webp'
          />
          <source srcSet={`https://${CDN_HOSTNAME}/${imageId}/-/format/auto/-/quality/best/-/scale_crop/480x417/center/-/sharp/5/,
                           https://${CDN_HOSTNAME}/${imageId}/-/format/auto/-/quality/normal/-/scale_crop/686x596/center/ 2x,
                           https://${CDN_HOSTNAME}/${imageId}/-/format/auto/-/quality/lighter/-/stretch/off/-/scale_crop/1372x1192/center/ 4x`}
            media='(max-width: 991px)'
          />
          <source
            srcSet={`https://${CDN_HOSTNAME}/${imageId}/-/format/webp/-/quality/best/-/scale_crop/416x344/center/-/sharp/5/,
                     https://${CDN_HOSTNAME}/${imageId}/-/format/webp/-/quality/normal/-/scale_crop/520x430/center/ 2x,
                     https://${CDN_HOSTNAME}/${imageId}/-/format/webp/-/quality/lighter/-/stretch/off/-/scale_crop/1040x860/center/ 4x`}
            type='image/webp'
          />
          <img
            src={`https://${CDN_HOSTNAME}/${imageId}/-/format/auto/-/quality/best/-/scale_crop/416x344/center/-/sharp/5/`}
            srcSet={`https://${CDN_HOSTNAME}/${imageId}/-/format/auto/-/quality/normal/-/scale_crop/520x430/center/ 2x,
                     https://${CDN_HOSTNAME}/${imageId}/-/format/auto/-/quality/lighter/-/stretch/off/-/scale_crop/1040x860/center/ 4x`}
            alt={alt}
            className={className}
            onLoad={onLoad}
            onError={onError}
          />
        </picture>
      );
    case 'previewMedium':
      return (
        <picture>
          <img
            src={`https://${CDN_HOSTNAME}/${imageId}/-/format/auto/-/quality/better/-/resize/747x/`}
            srcSet={`https://${CDN_HOSTNAME}/${imageId}/-/format/auto/-/quality/lighter/-/stretch/off/-/resize/1494x/ 2x,
                     https://${CDN_HOSTNAME}/${imageId}/-/format/auto/-/quality/lightest/-/stretch/off/-/resize/2988x/ 4x`}
            alt={alt}
            className={className}
            onLoad={onLoad}
            onError={onError}
          />
        </picture>
      );
    case 'fullSize':
      return (
        <picture>
          <img
            src={`https://${CDN_HOSTNAME}/${imageId}/-/format/auto/-/quality/best/`}
            alt={alt}
            className={className}
            onLoad={onLoad}
            onError={onError}
          />
        </picture>
      );
    default:
      return null;
  }
};

WorkImage.propTypes = {
  imageId: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
  className: PropTypes.string,
  alt: PropTypes.string,
  onLoad: PropTypes.func,
  onError: PropTypes.func,
};

WorkImage.defaultProps = {
  alt: '',
};

export default WorkImage;
