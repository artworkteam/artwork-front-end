import React from 'react';
import styles from './content.css';


const Content = ({ children }) => (
  <div className={styles.content}>
    { children }
  </div>
);

Content.propTypes = {
  children: React.PropTypes.node.isRequired,
};

export default Content;
