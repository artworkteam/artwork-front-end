import React from 'react';
import PropTypes from 'prop-types';
import { reduxForm } from 'redux-form';
import { Button } from '../../components/';
import { OkIcon, CancelIcon } from '../Icons/Icons';
import TextAreaField from '../TextAreaField/TextAreaField';
import ErrorField from '../ErrorField/ErrorField';
import Form from '../Form/Form';
import validation, { valid, notEmpty, size } from '../../utils/validation';
import styles from './workDescriptionForm.css';

const WorkDescriptionForm = ({ closeForm, ...props }) => (
  <Form className={styles.workDescriptionForm} {...props}>
    <TextAreaField
      className={styles.editDescription}
      name='description'
      autoFocus
      maxRows={20}
    />
    <ErrorField name='description' />
    <div className={styles.buttons}>
      <Button type="submit" styleType="square">
        <OkIcon />
      </Button>
      <Button styleType="squareRed" onClick={closeForm} >
        <CancelIcon />
      </Button>
    </div>
  </Form>
);

WorkDescriptionForm.propTypes = {
  closeForm: PropTypes.func.isRequired,
};

export default reduxForm({
  form: 'workDescription',
  validate: validation({
    description: valid(notEmpty(), size(0, 1000)),
  }),
})(WorkDescriptionForm);
