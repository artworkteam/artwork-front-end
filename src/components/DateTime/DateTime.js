import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { formatDate } from '../../utils';
import styles from './dateTime.css';

class DateTime extends Component {
  static propTypes = {
    className: PropTypes.string,
    timestamp: PropTypes.number,
  };

  constructor(props) {
    super(props);

    this.state = {
      displayDate: formatDate(props.timestamp),
    };
  }

  tick = () => {
    const newDisplayDate = formatDate(this.props.timestamp);
    const oldDisplayDate = this.state.displayDate;

    if (oldDisplayDate !== newDisplayDate) {
      this.setState({
        displayDate: newDisplayDate,
      });
    }
  }

  componentDidMount() {
    this.interval = setInterval(this.tick, 5 * 1000);
  }

  componentWillReceiveProps(nextProps) {
    const { timestamp } = this.props;

    if (timestamp !== nextProps.timestamp) {
      this.setState({
        displayDate: formatDate(nextProps.timestamp),
      });
    }
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  render() {
    const { className } = this.props;
    const { displayDate } = this.state;

    return (
      <span className={className || styles.dateTime}>{ displayDate }</span>
    );
  }
}

export default DateTime;
