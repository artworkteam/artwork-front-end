import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Button, SuggestionCommentForm, DateTime } from '../../components';
import { selectSuggestion, hidePreview, markSuggestionReaded } from '../../actions/SuggestionsActions';
import styles from './suggestionMarker.css';

export default class SuggestionMarker extends Component {
  static propTypes = {
    dispatch: PropTypes.func.isRequired,
    workId: PropTypes.string.isRequired,
    work: PropTypes.shape({}),
    suggestionId: PropTypes.string,
    suggestion: PropTypes.shape({}),
    selectedSuggestion: PropTypes.string,
    x: PropTypes.number.isRequired,
    y: PropTypes.number.isRequired,
  };

  mouseEnter = () => {
    const { suggestionId, dispatch } = this.props;

    if (suggestionId) {
      dispatch(selectSuggestion(suggestionId));
    }
  }

  componentWillReceiveProps(nextProps) {
    const { dispatch, work, workId, suggestion, selectedSuggestion, suggestionId } = this.props;
    const nextSelectedSuggestion = nextProps.selectedSuggestion;

    if (nextSelectedSuggestion
      && selectedSuggestion !== nextSelectedSuggestion
      && suggestionId === nextSelectedSuggestion
      && work.isMine
      && !suggestion.isMine
      && !suggestion.read
    ) {
      dispatch(markSuggestionReaded(workId, suggestionId));
    }
  }


  render() {
    const { suggestion, x, y } = this.props;
    const markerStyle = suggestion && suggestion.read
    ? styles.markerReaded
    : styles.suggestionMarker;


    return (
      <div
        className={!suggestion || suggestion.isMine
          ? styles.suggestionMarkerMyOwn
          : markerStyle
        }
        onMouseEnter={this.mouseEnter}
        style={{
          left: `${x}%`,
          top: `${y}%`,
        }}
      />
    );
  }
}
