import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router';
import styles from './worksSorting.css';

const WorksSorting = ({ filter, byUser }) => (
  <div className={styles.worksSorting}>
    <div className={styles.wrapper}>
      <ul className={styles.switcher}>
        <li className={filter === 'date' ? styles.dataActive : ''}>
          <Link
            className={styles.dateLink}
            to={{
              pathname: '/works',
              query: {
                sort: 'date',
                author: byUser,
              },
            }}
          >
            По дате
          </Link>
        </li>
        <li className={filter === 'rating' ? styles.ratingActive : ''}>
          <Link
            className={styles.ratingLink}
            to={{
              pathname: '/works',
              query: {
                sort: 'rating',
                author: byUser,
              },
            }}
          >
            По рейтингу
          </Link>
        </li>
        <li className={filter === 'popularity' ? styles.popularityActive : ''}>
          <Link
            className={styles.popularityLink}
            to={{
              pathname: '/works',
              query: {
                sort: 'popularity',
                author: byUser,
              },
            }}
          >
            По популярности
          </Link>
        </li>
        <hr className={styles.underscore} />
      </ul>
    </div>
  </div>
);

WorksSorting.propTypes = {
  filter: PropTypes.string,
  byUser: PropTypes.string,
};

WorksSorting.defaultProps = {
  filter: 'date',
  byUser: undefined,
};

export default WorksSorting;
