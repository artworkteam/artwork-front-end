import React, { Component } from 'react';
import PropTypes from 'prop-types';
import scrollToComponent from 'react-scroll-to-component';
import { WorkComment } from '../../components/';
import styles from './workCommentsList.css';

export default class WorkCommentsList extends Component {
  static propTypes = {
    workId: PropTypes.string.isRequired,
    comments: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
    selectedComment: PropTypes.string,
    users: PropTypes.objectOf(PropTypes.shape({})).isRequired,
    onCommentLike: PropTypes.func,
  };

  componentDidMount() {
    if (this.selectedRef) {
      setTimeout(() => scrollToComponent(this.selectedRef, {
        offset: 1000,
        duration: 1000,
      }), 200);
    }
  }

  render() {
    const { workId, comments, users, onCommentLike, selectedComment } = this.props;

    return (
      <div>
        {
          comments.map((comment, index, array) => {
            const previousItem = array[index - 1];
            if (comment.relatesToSuggestion
               && previousItem
               && previousItem.relatesToSuggestion
               && comment.author === previousItem.author
             ) {
              return null;
            }

            const isSelected = selectedComment === comment.commentId;

            return (
              <WorkComment
                className={isSelected ? styles.selectedComment : ''}
                workId={workId}
                key={comment.commentId}
                comment={comment}
                author={users[comment.author]}
                onLike={onCommentLike && (() => onCommentLike(comment))}
                ref={(ref) => {
                  if (isSelected) {
                    this.selectedRef = ref;
                  }
                }}
              />
            );
          })
        }
      </div>
    );
  }
}
