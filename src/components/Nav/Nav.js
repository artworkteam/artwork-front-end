import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router';
import { Button, Logo, Avatar } from '../../components/';
import { TurnOffIcon } from '../Icons/Icons';
import { logoutAuthedUser } from '../../actions/AuthActions';
import { showModal } from '../../actions/ModalActions';
import SseNotifications from '../../containers/SseNotificationsContainer';
import styles from './nav.css';

export default class Nav extends Component {
  static propTypes = {
    dispatch: PropTypes.func.isRequired,
    user: PropTypes.shape({}),
    isMainPage: PropTypes.bool.isRequired,
    isMineWorksPage: PropTypes.bool.isRequired,
    isWorksByUserPage: PropTypes.bool.isRequired,
  };

  static defaultProps = {
    user: undefined,
  };

  state = {
    menuMobileExpanded: false,
  };

  componentDidMount() {
    document.addEventListener('mousedown', this.onOutsideClick);
  }

  componentWillUnmount() {
    document.removeEventListener('mousedown', this.onOutsideClick);
  }

  onOutsideClick = (e) => {
    if (!this.state.menuMobileExpanded) {
      return;
    }

    e.stopPropagation();
    const localNode = this.navRef;
    let source = e.target;

    while (source.parentNode) {
      if (source === localNode) {
        return;
      }
      source = source.parentNode;
    }

    this.hideMenu();
  }

  menuExpandClick = (e) => {
    e.preventDefault();
    this.setState({
      menuMobileExpanded: !this.state.menuMobileExpanded,
    });
  }

  hideMenu = () => {
    if (this.state.menuMobileExpanded) {
      this.setState({
        menuMobileExpanded: false,
      });
    }
  }

  menuItemClick = () => {
    this.hideMenu();
  }

  addWorkClick = (e) => {
    e.preventDefault();
    this.menuItemClick();
    const { dispatch } = this.props;

    dispatch(showModal('addWork'));
  }

  loginClick = (e) => {
    e.preventDefault();
    this.menuItemClick();
    const { dispatch } = this.props;
    dispatch(showModal('login'));
  }

  logoutClick = (e) => {
    e.preventDefault();
    this.menuItemClick();
    const { dispatch } = this.props;

    dispatch(logoutAuthedUser());
  }

  renderMenu() {
    const { user, isMainPage, isMineWorksPage, isWorksByUserPage } = this.props;
    const menuClassName = this.state.menuMobileExpanded ?
        styles.menuMobileExpanded : styles.menuMobileCollapsed;

    if (!user) {
      return (
        <ul className={menuClassName}>
          <li className={styles.about}>
            <Link
              className={styles.unloginedAboutLink}
              to={'/about'}
              onClick={this.menuItemClick}
              activeClassName={styles.activeLink}
              replace
            >
              О проекте
            </Link>
          </li>
          <div className={styles.space} />
          <li className={styles.login}>
            <div className={styles.loginLink}>
              <Button styleType='basic' onClick={this.loginClick}>
                Войти
              </Button>
            </div>
          </li>
        </ul>
      );
    }

    return (
      <ul className={menuClassName}>
        <li className={styles.allWorks}>
          <Link
            className={isMainPage && !isWorksByUserPage ? styles.activeLink : styles.link}
            to='/works'
            onClick={this.menuItemClick}
            replace
          >
            Все работы
           </Link>
        </li>
        <li className={styles.myWorks}>
          <Link
            className={isMainPage && isMineWorksPage ? styles.activeLink : styles.link}
            to={{
              pathname: '/works',
              query: { author: user.uuid },
            }}
            onClick={this.menuItemClick}
            replace
          >
            Мои работы
          </Link>
        </li>
        <li className={styles.about}>
          <Link
            className={styles.link}
            to={'/about'}
            onClick={this.menuItemClick}
            activeClassName={styles.activeLink}
            replace
          >
            О проекте
          </Link>
        </li>
        <div className={styles.space} />
        <li className={styles.addWork}>
          <a className={styles.addWorkLink} onClick={this.addWorkClick} href='#'>
            Добавить работу
          </a>
        </li>
        <li className={styles.user} >
          <Link
            className={styles.userLink}
            to={{
              pathname: '/works',
              query: { author: user.uuid },
            }}
            onClick={this.menuItemClick}
            replace
          >
            <div className={styles.avatar}>
              <Avatar
                user={user}
                size={'small'}
                link={false}
              />
            </div>
            <div className={styles.username}>{user.displayName}</div>
          </Link>
        </li>
        <li className={styles.recentInfo}>
          <SseNotifications onListHidden={this.hideMenu} />
        </li>
        <li className={styles.logout}>
          <Button
            styleType='clear'
            className={styles.logoutLink}
            onClick={this.logoutClick}
          >
            <div className={styles.logoutText}>Выход</div>
            <div className={styles.logoutIcon}>
              <TurnOffIcon />
            </div>
          </Button>
        </li>
      </ul>
    );
  }

  render() {
    const { user } = this.props;

    return (
      <nav className={styles.nav} ref={(ref) => { this.navRef = ref; }}>
        <div className={styles.wrapper}>
          <div className={styles.header}>
            <Link to='/' className={user ? styles.logo : styles.guestLogo}>
              <Logo />
            </Link>
            <button type='button' className={styles.button} onClick={this.menuExpandClick}>
              <span className={styles.buttonBar} />
              <span className={styles.buttonBar} />
              <span className={styles.buttonBar} />
            </button>
          </div>
          { this.renderMenu() }
        </div>
      </nav>
    );
  }
}
