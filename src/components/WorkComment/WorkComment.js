import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { Link } from 'react-router';
import nl2br from 'react-nl2br';
import { DateTime, Avatar, Username } from '../../components';
import LikeButton from '../LikeButton/LikeButton';
import styles from './workComment.css';

export default class WorkComment extends Component {
  static propTypes = {
    workId: PropTypes.string.isRequired,
    comment: PropTypes.shape({
      createdDate: PropTypes.number,
      text: PropTypes.string,
      likesCount: PropTypes.number,
      relatesToSuggestion: PropTypes.bool,
    }).isRequired,
    author: PropTypes.shape({
      profileUrl: PropTypes.string,
      imageUrl: PropTypes.string,
      displayName: PropTypes.string,
    }).isRequired,
    onLike: PropTypes.func,
    className: PropTypes.string,
  };

  render() {
    const { workId, comment, author, onLike, className } = this.props;

    if (comment.relatesToSuggestion) {
      return (
        <div className={styles.workCommentToSuggestion}>
          <div className={styles.left}>
            <Avatar
              className={styles.avatarLink}
              user={author}
            />
          </div>
          <div className={styles.body}>
            <div className={styles.head}>
              <Avatar
                className={styles.mobileAvatarLink}
                user={author}
                size={'small'}
              />
              <div className={styles.name}>
                <Username user={author} />
              </div>
              <DateTime className={styles.date} timestamp={comment.createdDate} />
            </div>
            <div className={styles.viewSuggestionBlock}>
              <i className={styles.viewSuggestionText}>
                Порекомендовал(а) изменения на работе…
              </i>
              <Link className={styles.viewSuggestionLink} to={{
                pathname: `/works/${workId}/suggestions`,
                query: { author: author.uuid },
                replace: true,
              }}
              >Посмотреть</Link>
            </div>
          </div>
        </div>
      );
    }

    return (
      <div className={classNames(styles.workComment, className)}>
        <div className={styles.left}>
          <Avatar
            className={styles.avatarLink}
            user={author}
          />
        </div>
        <div className={styles.body}>
          <div className={styles.head}>
            <Avatar
              className={styles.mobileAvatarLink}
              user={author}
              size={'small'}
            />
            <div className={styles.name}>
              <Username user={author} />
            </div>
            <DateTime className={styles.date} timestamp={comment.createdDate} />
          </div>
          <p className={styles.text}>
            {nl2br(comment.text)}
          </p>
          <LikeButton
            likesCount={comment.likesCount}
            alreadyLiked={comment.alreadyLiked}
            onLike={onLike}
          />
        </div>
      </div>
    );
  }
}
