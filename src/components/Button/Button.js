import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import styles from './button.css';

const Button = ({ children, styleType, className, ...rest }) => (
  <button {...rest} className={classNames(styles[styleType], className)}>
    {styleType === "close" && "×"}
    { children }
  </button>
);

Button.propTypes = {
  children: PropTypes.node,
  styleType: PropTypes.string.isRequired,
};

export default Button;
