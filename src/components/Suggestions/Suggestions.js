import React, { Component } from 'react';
import PropTypes from 'prop-types';
import BodyClassName from 'react-body-classname';
import find from 'lodash/find';
import ReactResizeDetector from 'react-resize-detector';
import { SuggestionsImage, SuggestionAuthors, LoaderSpinner, ModalCloseButton } from '../../components';
import { initSuggestions, selectSuggestion } from '../../actions/SuggestionsActions';
import { push } from '../../actions/RouteActions';
import styles from './suggestions.css';

export default class Suggestions extends Component {
  static propTypes = {
    dispatch: PropTypes.func.isRequired,
    me: PropTypes.shape({}),
    workId: PropTypes.string.isRequired,
    works: PropTypes.objectOf(PropTypes.shape({})).isRequired,
    suggestionsByAuthor: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
    isAddSuggestionMode: PropTypes.bool.isRequired,
    authorQuery: PropTypes.string,
    initial: PropTypes.bool.isRequired,
    querySuggestion: PropTypes.string,
  };

  state = {
    imageContainerWidth: window.innerWidth - 300,
    imageContainerHeight: window.innerHeight,
  }

  onResize = (width, height) => {
    this.setState({
      imageContainerWidth: width,
      imageContainerHeight: height,
    });
  }

  componentWillMount() {
    const { dispatch, workId, querySuggestion } = this.props;

    dispatch(initSuggestions(workId));
    if (querySuggestion) {
      dispatch(selectSuggestion(querySuggestion));
    }
  }

  closeClick = () => {
    const { dispatch, workId } = this.props;

    dispatch(push(`/works/${workId}`));
  }

  render() {
    const { initial, workId, works } = this.props;
    const work = works[workId];

    if (initial) {
      return (
        <div className={styles.spinnerContainer}>
          <LoaderSpinner />
        </div>
      );
    }

    const { suggestionsByAuthor, authorQuery, me, isAddSuggestionMode } = this.props;
    const { imageContainerWidth, imageContainerHeight } = this.state;
    const authorByQueryOrFirst = authorQuery
    ? find(suggestionsByAuthor, o => o.author.uuid === authorQuery)
    : suggestionsByAuthor[0];
    const selectedAuthor = isAddSuggestionMode && me
    ? find(suggestionsByAuthor, o => o.author.uuid === me.uuid)
    : authorByQueryOrFirst;

    return (
      <BodyClassName className='noScroll'>
        <div className={styles.suggestions}>
          <div className={styles.closeButton}>
            <ModalCloseButton closeModal={this.closeClick} />
          </div>
          <div
            className={styles.left}
            ref={(ref) => { this.imageContainerRef = ref; }}
          >
            <ReactResizeDetector handleWidth handleHeight onResize={this.onResize} />
            <SuggestionsImage
              {...this.props}
              work={work}
              selectedAuthor={selectedAuthor}
              isAdduggestionMode={isAddSuggestionMode}
              me={me}
              imageContainerWidth={imageContainerWidth}
              imageContainerHeight={imageContainerHeight}
              imageContainerRef={this.imageContainerRef}
            />
          </div>
          <div className={styles.right}>
            <div className={styles.userTips}>
              <SuggestionAuthors
                {...this.props}
                work={work}
                selectedAuthor={selectedAuthor}
                isAddSuggestionMode={isAddSuggestionMode}
                me={me}
              />
            </div>
            <p className={styles.resolutionWarningText}>
              Вам нужен экран большего разрешения, что бы просматривать рекомендации
            </p>
          </div>
        </div>
      </BodyClassName>
    );
  }
}
