import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router';
import Waypoint from 'react-waypoint';
import classNames from 'classnames';
import { BackButton } from '../../components';
import Avatar from '../Avatar/Avatar';
import LoaderSpinner from '../LoaderSpinner/LoaderSpinner';
import { formatTime, formatDate } from '../../utils/formatDate';
import styles from './NotificationsTimeline.css';

export default class NotificationsTimeline extends Component {
  static propTypes = {
    notifications: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
    hasMore: PropTypes.bool,
    nextPage: PropTypes.number.isRequired,
    loading: PropTypes.bool.isRequired,
    loadMore: PropTypes.func.isRequired,
  };

  renderSeparator = (date, isFirst) => (
    <div key={date} className={isFirst ? styles.firstSeparator : styles.separator}>
      {date}
    </div>
  );

  showNotificationText = ({ type }) => {
    switch (type) {
      case 'WORK_COMMENTED':
      case 'SUGGESTION_CREATED':
        return true;
      default:
        return false;
    }
  };

  getTypeText = ({ type }) => {
    switch (type) {
      case 'WORK_COMMENTED':
        return 'Добавил(а) комментарий к вашей работе';
      case 'SUGGESTION_CREATED':
        return 'Оставил(а) отметку на вашей работе';
      case 'WORK_LIKED':
        return 'Оценил(а) вашу работу';
      case 'COMMENT_LIKED':
        return 'Оценил(а) ваш коментарий';
      default:
        return '';
    }
  };

  linkToNotificationTrigger = (notification) => {
    const { type, workId, commentId, suggestionId, initiator } = notification;

    switch (type) {
      case 'WORK_COMMENTED':
        return `/works/${workId}?comment=${commentId}`;
      case 'SUGGESTION_CREATED':
        return `/works/${workId}/suggestions/?author=${initiator.uuid}&suggestion=${suggestionId}`;
      case 'WORK_LIKED':
        return `/works/${workId}`;
      case 'COMMENT_LIKED':
        return `/works/${workId}?comment=${commentId}`;
      default:
        return `/works/${workId}`;
    }
  }

  renderNotification = n => (
    <section key={n.notificationId} className={styles.card}>
      <div className={styles.side}>
        <div className={styles.body}>
          <div className={styles.head}>
            <span className={styles.name}>{n.initiator.displayName}</span>
          </div>
          <div className={styles.type}>{this.getTypeText(n)}</div>
          {this.showNotificationText(n) &&
            <p className={styles.text}>
              {n.text}
            </p>
          }
          <Link
            className={styles.link}
            to={this.linkToNotificationTrigger(n)}
          >
            Посмотреть
          </Link>
        </div>
        <Avatar
          className={styles.author}
          user={n.initiator}
        />
      </div>
      <div className={styles.circle}>
        <div className={classNames(!n.read && styles.new)} />
      </div>
      <div className={styles.side}>
        <div className={classNames(styles.time, !n.read && styles.new)}>
          {formatTime(n.createdDate)}
        </div>
      </div>
    </section>
  );

  renderLoader = () => <LoaderSpinner />;

  renderNextPageLoader = () => (
    <div className={styles.loadNextPageSpinner}>
      <LoaderSpinner />
    </div>
  );

  onEnter = () => {
    const { loading, loadMore } = this.props;
    if (!loading) loadMore();
  };

  render() {
    const { notifications, hasMore, nextPage, loading } = this.props;

    if (loading && nextPage === 0) {
      return (<div className={styles.spinner}>{this.renderLoader()}</div>);
    }

    const elements = [];
    let lastDate = null;
    notifications.forEach((n) => {
      const date = formatDate(n.createdDate);
      if (date !== lastDate) {
        const isFirst = lastDate === null;
        lastDate = date;
        elements.push(this.renderSeparator(date, isFirst));
      }
      elements.push(this.renderNotification(n));
    });

    return (
      <div className={styles.notificationsTimeline}>
        <BackButton />
        <div className={classNames(styles.wrapper, notifications.length > 0 && styles.line)}>
          {elements}
          {hasMore &&
            <Waypoint
              key={notifications.length}
              onEnter={this.onEnter}
              scrollableAncestor={window}
            />
          }
          {!hasMore && notifications.length === 0 &&
            <span className={styles.empty}>У вас нет уведомлений</span>
          }
        </div>
        {loading && this.renderNextPageLoader()}
      </div>
    );
  }
}
