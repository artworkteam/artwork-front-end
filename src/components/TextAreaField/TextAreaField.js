import React from 'react';
import PropTypes from 'prop-types';
import TextareaAutosize from 'react-textarea-autosize';
import classNames from 'classnames';
import { Field } from 'redux-form';

import styles from './TextAreaField.css';

const RenderTextAreaField = ({ className, input, meta: { touched, invalid }, ...props }) => (
  <TextareaAutosize
    className={classNames(className, styles.input, { [styles.invalid]: touched && invalid })}
    {...input}
    {...props}
  />
);

RenderTextAreaField.propTypes = {
  className: PropTypes.string,
  input: PropTypes.shape({
    value: PropTypes.string,
  }),
  meta: PropTypes.shape({
    invalid: PropTypes.bool,
  }),
  disabled: PropTypes.bool,
};

const TextAreaField = ({ name, ...props }) => (
  <Field
    name={name}
    component={RenderTextAreaField}
    {...props}
  />
);
export default TextAreaField;

TextAreaField.propTypes = {
  name: PropTypes.string.isRequired,
};
