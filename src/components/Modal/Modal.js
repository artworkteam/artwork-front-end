import React, { Component } from 'react';
import PropTypes from 'prop-types';
import * as ReactModal from 'react-modal';
import { hideModal } from '../../actions/ModalActions';
import { ModalAddWork, ModalLogin, ModalDeleteWork } from '../../components';
import styles from './modal.css';

export default class Modal extends Component {
  static propTypes = {
    modalType: PropTypes.string.isRequired,
    dispatch: PropTypes.func.isRequired,
    params: PropTypes.shape({}).isRequired,
  }

  closeModal = () => {
    const { dispatch } = this.props;

    dispatch(hideModal());
  }

  renderContent() {
    const { dispatch, modalType, params } = this.props;
    switch (modalType) {
      case 'login':
        return <ModalLogin closeModal={this.closeModal} dispatch={dispatch} />;
      case 'addWork':
        return <ModalAddWork closeModal={this.closeModal} />;
      case 'deleteWork':
        return <ModalDeleteWork closeModal={this.closeModal} dispatch={dispatch} params={params} />;
      default:
        return null;
    }
  }

  render() {
    const { modalType } = this.props;

    return (
      <ReactModal
        isOpen
        onRequestClose={this.closeModal}
        contentLabel='Modal'
        className={styles[modalType] || styles.basic}
        overlayClassName={styles.overlay}
      >
        { this.renderContent() }
      </ReactModal>
    );
  }
}
