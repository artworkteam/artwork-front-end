import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { clearWorks, getWorks, likeWork } from '../../actions/WorksActions';
import { getUser } from '../../actions/UsersActions';
import { WorksSorting, UserSection, WorkCard } from '../../components/';
import InfiniteGrid from '../InfiniteGrid/InfiniteGrid';
import LoaderSpinner from '../LoaderSpinner/LoaderSpinner';
import styles from './works.css';

export default class Works extends Component {
  static propTypes = {
    works: PropTypes.objectOf(PropTypes.shape({})).isRequired,
    worksSelected: PropTypes.arrayOf(PropTypes.string).isRequired,
    dispatch: PropTypes.func.isRequired,
    filter: PropTypes.string,
    byUser: PropTypes.string,
    users: PropTypes.objectOf(PropTypes.shape({})).isRequired,
    loading: PropTypes.bool.isRequired,
    nextPage: PropTypes.number.isRequired,
    hasMore: PropTypes.bool.isRequired,
    auth: PropTypes.shape({
      user: PropTypes.shape({}),
    }),
  };

  static defaultProps = {
    filter: 'date',
    byUser: undefined,
  }

  componentWillReceiveProps(nextProps) {
    const { dispatch, filter, byUser } = this.props;
    if (filter !== nextProps.filter || byUser !== nextProps.byUser) {
      dispatch(clearWorks());
    }
    if (nextProps.byUser && byUser !== nextProps.byUser) {
      dispatch(getUser(nextProps.byUser));
    }
  }

  componentWillMount() {
    const { dispatch, byUser } = this.props;
    dispatch(clearWorks());
    if (byUser) dispatch(getUser(byUser));
  }

  onLike = (work) => {
    if (this.props.auth.user) {
      this.props.dispatch(likeWork(work.workId, !work.alreadyLiked));
    }
  };

  renderWorkCard = (id) => {
    const { works, users } = this.props;
    const work = works[id];
    const owner = users[work.owner];
    return (
      <div key={work.workId} className={styles.workCard}>
        <WorkCard work={work} owner={owner} onLike={() => this.onLike(work)} />
      </div>
    );
  };
  renderLoading = () => <div className={styles.loader}><LoaderSpinner /></div>;
  renderPlaceholder = () => <span>Здесь пока еще нет работ.</span>;
  loadMore = () => {
    const { dispatch, nextPage, filter, byUser } = this.props;
    dispatch(getWorks({ page: nextPage, filter, byUser }));
  };

  render() {
    const { byUser, loading, users, worksSelected, hasMore, nextPage } = this.props;
    const user = users[byUser];

    return (
      <div className={styles.works}>
        <div className={styles.worksSorting}>
          <WorksSorting {...this.props} />
        </div>

        { byUser && (!!nextPage || !!worksSelected.length) &&
          <div className={styles.userSection}>
            <UserSection user={user} />
          </div>
        }
        <InfiniteGrid
          className={styles.wrapper}
          items={worksSelected}
          renderItem={this.renderWorkCard}
          renderLoading={this.renderLoading}
          renderPlaceholder={this.renderPlaceholder}
          hasMore={hasMore}
          loading={loading}
          loadMore={this.loadMore}
        />
      </div>
    );
  }
}
