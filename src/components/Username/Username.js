import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { Link } from 'react-router';
import styles from './username.css';


const Username = ({ className, link, user, styleType, color, ...rest }) => {
  if (!link) {
    return (
      <div
        {...rest}
        className={classNames(styles[styleType], className, styles.noLink)}
        style={{
          color,
        }}
      >
        { user.displayName }
      </div>
    );
  }

  return (
    <Link
      {...rest}
      className={classNames(styles[styleType], className)}
      style={{
        color,
      }}
      to={{
        pathname: '/works',
        query: { author: user.uuid },
      }}
      replace
    >
      { user.displayName }
    </Link>
  );
};

Username.propTypes = {
  className: PropTypes.string,
  link: PropTypes.bool,
  user: PropTypes.shape({
    uuid: PropTypes.string.isRequired,
    displayName: PropTypes.string.isRequired,
  }).isRequired,
  styleType: PropTypes.string,
  color: PropTypes.string,
};

Username.defaultProps = {
  styleType: 'blue',
  color: null,
  link: true,
};

export default Username;
