import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { MegaphoneIcon } from '../Icons/Icons';
import { ERROR_ALERT, SUCCESS_ALERT } from '../../constants/Alerts';
import styles from './alert.css';


export default class Alert extends Component {
  static propTypes = {
    type: PropTypes.string.isRequired,
    message: PropTypes.string.isRequired,
    clearAlert: PropTypes.func.isRequired,
  };

  stylesByType = {
    [ERROR_ALERT]: {
      icon: styles.iconError,
      content: styles.contentError,
    },
    [SUCCESS_ALERT]: {
      icon: styles.iconSucces,
      content: styles.contentSucces,
    },
  };

  componentDidMount() {
    const { clearAlert } = this.props;
    this.clearTimeout = setTimeout(() => {
      clearAlert();
    }, 5000);
  }

  componentWillUnmount() {
    clearTimeout(this.clearTimeout);
  }

  render() {
    const { type, message, clearAlert } = this.props;

    return (
      <div className={styles.alert}>
        <div className={this.stylesByType[type].icon}>
          <MegaphoneIcon />
        </div>
        <div className={this.stylesByType[type].content}>
          <div className={styles.message}>{message}</div>
          <div className={styles.close}><span onClick={clearAlert} /></div>
        </div>
      </div>
    );
  }
}
