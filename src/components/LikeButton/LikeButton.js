import React from 'react';
import PropTypes from 'prop-types';
import { Button } from '../../components';
import { HeartIcon } from '../Icons/Icons';
import styles from './likeButton.css';

const LikeButton = ({ likesCount, alreadyLiked, onLike }) => (
  <Button
    styleType='clear'
    className={alreadyLiked ? styles.likeButtonLiked : styles.likeButton}
    onClick={onLike}
  >
    <div className={styles.icon}>
      <HeartIcon fillColor={alreadyLiked ? '#d8536f' : ''} />
    </div>
    {likesCount}
  </Button>
);

LikeButton.propTypes = {
  likesCount: PropTypes.number.isRequired,
  alreadyLiked: PropTypes.bool,
  onLike: PropTypes.func,
};

export default LikeButton;
