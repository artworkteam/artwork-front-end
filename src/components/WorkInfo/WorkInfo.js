import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router';
import nl2br from 'react-nl2br';
import { showModal } from '../../actions/ModalActions';
import { WorkDescriptionForm, Button, Icon, DateTime, Avatar, Username } from '../../components/';
import { PencilIcon, DeleteIcon } from '../Icons/Icons';
import styles from './workInfo.css';

export default class WorkInfo extends Component {
  static propTypes = {
    owner: PropTypes.shape({}).isRequired,
    work: PropTypes.shape({}).isRequired,
    me: PropTypes.shape({
      uuid: PropTypes.string,
    }),
    onWorkDescriptionSubmit: PropTypes.func.isRequired,
    dispatch: PropTypes.func.isRequired,
  };

  state = {
    editable: false,
  };

  closeForm = (e) => {
    if (e && e.preventDefault) e.preventDefault();
    this.setState({
      editable: false,
    });
  };

  isOwner = () => {
    const { owner, me } = this.props;
    return owner && me && owner.uuid === me.uuid;
  };

  editClick = () => {
    this.setState({
      editable: true,
    });
  };

  deleteClick = () => {
    const { dispatch } = this.props;

    dispatch(showModal('deleteWork'));
  }

  onEditSuccess = () => this.closeForm();

  render() {
    const { owner, work, onWorkDescriptionSubmit } = this.props;

    return (
      <div className={styles.workInfo}>
        <div className={styles.infoHeader}>
          <div className={styles.avatar}>
            <Avatar
              user={owner}
            />
          </div>
          <div className={styles.info}>
            <div className={styles.name}>
              <Username user={owner} />
            </div>
            <DateTime className={styles.date} timestamp={work.createdDate} />
          </div>
        </div>
        <div className={styles.dots} />
        <div className={styles.text}>
          { this.state.editable &&
            <WorkDescriptionForm
              initialValues={{ description: work.description }}
              closeForm={this.closeForm}
              onSubmit={onWorkDescriptionSubmit}
              onSubmitSuccess={this.onEditSuccess}
            />
          }
          { !this.state.editable &&
            <p>{nl2br(work.description)}</p>
          }
        </div>
        <div className={styles.dots} />
        { !this.state.editable && this.isOwner() &&
          <div className={styles.buttons}>
            <Button styleType='square' onClick={this.editClick}>
              <PencilIcon />
            </Button>
            <Button styleType='squareRed' onClick={this.deleteClick}>
              <DeleteIcon />
            </Button>
          </div>
        }
      </div>
    );
  }
}
