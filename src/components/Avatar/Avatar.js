import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router';
import classNames from 'classnames';
import styles from './Avatar.css';

const sizesClassNames = {
  small: styles.avatarImgSmall,
  medium: styles.avatarImgMedium,
  big: styles.avatarImgBig,
};

const Avatar = ({ className, user, size, link, customSize, ...rest }) => {
  const customSizeStyles = customSize
    ? { width: customSize, height: customSize }
    : null;

  if (!link) {
    return (
      <img
        src={user.imageUrl}
        alt=''
        className={classNames(sizesClassNames[size], styles.noHover)}
        style={customSizeStyles}
      />
    );
  }

  return (
    <Link
      {...rest}
      className={classNames(styles.avatar, className)}
      to={{
        pathname: '/works',
        query: { author: user.uuid },
      }}
      replace
    >
      <img
        src={user.imageUrl}
        alt=''
        className={sizesClassNames[size]}
        style={customSizeStyles}
      />
    </Link>
  );
};

Avatar.propTypes = {
  className: PropTypes.string,
  size: PropTypes.string,
  customSize: PropTypes.number,
  user: PropTypes.shape({
    uuid: PropTypes.string.isRequired,
    imageUrl: PropTypes.string.isRequired,
  }).isRequired,
  link: PropTypes.bool,
};

Avatar.defaultProps = {
  size: 'medium',
  customSizeStyles: null,
  link: true,
};

export default Avatar;
