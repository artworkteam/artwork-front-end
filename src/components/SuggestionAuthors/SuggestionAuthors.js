import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router';
import { DateTime, Button, Avatar, Username } from '../../components';
import { Chat2Icon } from '../Icons/Icons';
import { resetSelected } from '../../actions/SuggestionsActions';
import { push } from '../../actions/RouteActions';
import commentsIcon from './comments_i.svg';
import styles from './suggestionAuthors.css';

export default class SuggestionAuthors extends Component {
  static propTypes = {
    dispatch: PropTypes.func.isRequired,
    me: PropTypes.shape({}),
    workId: PropTypes.string.isRequired,
    work: PropTypes.shape({}).isRequired,
    suggestionsByAuthor: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
    selectedAuthor: PropTypes.shape({}).isRequired,
    isAddSuggestionMode: PropTypes.bool.isRequired,
  };

  authorClick = (e, authorId) => {
    const { dispatch, workId } = this.props;

    dispatch(resetSelected());
    dispatch(push(`/works/${workId}/suggestions?author=${authorId}`))
  }

  avatarClick = (e) => {
    e.stopPropagation();
  }

  renderAuthors() {
    const { suggestionsByAuthor, selectedAuthor, workId, work, isAddSuggestionMode, me, selectedSuggestion } = this.props;
    const authors = [];

    if (suggestionsByAuthor.length && isAddSuggestionMode && !selectedAuthor) {
      authors.push((
        <div className={styles.authorSelected}>
          <div className={styles.avatar}>
            <Avatar
              user={me}
              size={'big'}
              link={false}
            />
          </div>
          <div className={styles.info}>
            <div className={styles.name}>
              <Username
                user={me}
                styleType={'inherit'}
              />
            </div>
            <div className={styles.space} />
            <span className={styles.count}>0</span>
            <img src={commentsIcon} width='15px' height='12px' alt='' />
          </div>
        </div>
      ));
    }

    authors.push(suggestionsByAuthor.map((author) => {
      const isMine = author.suggestions[0].isMine;
      const isSelected = selectedAuthor && author.author.uuid === selectedAuthor.author.uuid;
      const unSelectedStyle = work.isMine && !isMine && me && author.hasNotRead ? styles.authorUnreaded : styles.author;

      return (
        <div
          className={isSelected ? styles.authorSelected : unSelectedStyle}
          onClick={e => this.authorClick(e, author.author.uuid)}
        >
          <div className={styles.avatar}>
            <Avatar
              user={author.author}
              size={'big'}
              onClick={e => e.stopPropagation()}
            />
          </div>
          <div className={styles.info}>
            <div className={styles.name}>
              <Username
                user={author.author}
                styleType={'inherit'}
                onClick={e => e.stopPropagation()}
              />
            </div>
            <DateTime className={styles.date} timestamp={author.lastUpdate} />
            <span className={styles.count}>{author.suggestionsCount}</span>
            <img src={commentsIcon} width='15px' height='12px' alt='' />
          </div>
        </div>
      );
    }));

    return authors;
  }

  renderAddingAdvice() {
    const { me } = this.props;
    return (
      <div className={styles.addingAdvice}>
        <Chat2Icon />
        <p className={styles.addingAdviceText}>
          {me ?
            'Для добавления рекомендации нажмите на изображение в нужном месте' :
            'Авторизуйтесь для добавления рекомендации'
          }
        </p>
      </div>
    );
  }

  render() {
    const { suggestionsByAuthor, isAddSuggestionMode } = this.props;
    if (!suggestionsByAuthor.length) {
      return this.renderAddingAdvice();
    }

    return (
      <div className={styles.suggestionAuthors}>
        { this.renderAuthors() }
      </div>
    );
  }
}
