import React from 'react';
import PropTypes from 'prop-types';
import { VkontakteIcon, FacebookIcon } from '../Icons/Icons';
import { PROVIDERS } from '../../constants/Config';
import { oAuthSignIn } from '../../actions/AuthActions';
import { hideModal } from '../../actions/ModalActions';
import styles from './authButton.css';

const AuthButton = ({ name, dispatch }) => (
  <a
    className={styles[name]} href='#' onClick={() => {
    dispatch(oAuthSignIn(name))
      .then(() => dispatch(hideModal()));
  }}
  >
    <div className={styles.icon}>
      { name === 'vkontakte' &&
      <VkontakteIcon />
      }
      { name === 'facebook' &&
      <FacebookIcon />
      }
    </div>
    { PROVIDERS[name].displayName }
  </a>
);

AuthButton.propTypes = {
  name: PropTypes.string.isRequired,
  dispatch: PropTypes.func.isRequired,
};

export default AuthButton;
