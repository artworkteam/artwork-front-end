import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router';
import { WorkCounters, DateTime, WorkImage, Avatar, Username } from '../../components/';
import styles from './workCard.css';


const WorkCard = ({ work, owner, onLike }) => (
  <div className={styles.workCard}>
    <div className={styles.container}>
      <div className={styles.descriptionContainer}>
        <div className={styles.description}>
          <div className={styles.header}>
            <div className={styles.avatar}>
              <Avatar
                user={owner}
                size={'small'}
              />
            </div>
            <div className={styles.info}>
              <div className={styles.name}>
                <Username user={owner} />
              </div>
              <DateTime className={styles.date} timestamp={work.createdDate} />
            </div>
          </div>
          <Link className={styles.textLink} to={`works/${work.workId}`}>
            <p className={styles.text}>{work.description}</p>
          </Link>
        </div>
        <div className={styles.imageContainer}>
          <WorkImage
            imageId={work.imageId}
            type='previewSmall'
            className={styles.image}
          />
        </div>
      </div>
      <div className={styles.counters}>
        <WorkCounters work={work} onLike={onLike} />
      </div>
    </div>
  </div>
);

WorkCard.propTypes = {
  work: PropTypes.shape({
    workId: PropTypes.string,
    createdDate: PropTypes.number,
    description: PropTypes.string,
  }).isRequired,
  owner: PropTypes.shape({
    uuid: PropTypes.string,
    imageUrl: PropTypes.string,
    displayName: PropTypes.string,
  }).isRequired,
  onLike: PropTypes.func.isRequired,
};

export default WorkCard;
