import React from 'react';
import PropTypes from 'prop-types';
import { Avatar } from '../../components';
import { FileImageIcon, ChatIcon, HeartIcon } from '../Icons/Icons';
import styles from './userSection.css';

const UserSection = ({ user }) => (
  <div className={styles.userSection}>
    {user &&
      <div className={styles.user}>
        <div className={styles.avatar}>
          <Avatar
            user={user}
            size={'big'}
            link={false}
          />
        </div>
        <div>
          <h4 className={styles.name}>{user.displayName}</h4>
          <a
            className={styles.socialUrl}
            href={user.profileUrl}
            target='_blank'
            rel='noreferrer noopener'
          >
            Профиль в соц. сети
          </a>
        </div>
      </div>
    }
    { user && user.worksCount !== undefined &&
    <div className={styles.info}>
      <div className={styles.works}>
        <div className={styles.icon}>
          <FileImageIcon />
        </div>
        <span className={styles.num}>{user.worksCount}</span>
      </div>
      <div className={styles.comments}>
        <div className={styles.icon}>
          <ChatIcon width={'25'} height={'25'} fillColor={'#62686d'} />
        </div>
        <span className={styles.num}>{user.commentsCount}</span>
      </div>
      <div className={styles.likes}>
        <div className={styles.icon}>
          <HeartIcon width={'25'} height={'25'} fillColor={'#62686d'} />
        </div>
        <span className={styles.num}>{user.likesCount}</span>
      </div>
    </div>
    }
  </div>
);

UserSection.propTypes = {
  user: PropTypes.shape({
    displayName: PropTypes.string,
  }),
};

export default UserSection;
