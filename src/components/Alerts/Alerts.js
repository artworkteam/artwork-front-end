import React from 'react';
import PropTypes from 'prop-types';
import { CSSTransitionGroup } from 'react-transition-group';
import { Alert } from '../../components/';
import styles from './alerts.css';

//will possible to render few alerts later
const Alerts = ({ type, message, clearAlert }) => (
  <CSSTransitionGroup
    transitionName={{
      enter: styles.enter,
      enterActive: styles.enterActive,
      leave: styles.leave,
      leaveActive: styles.leaveActive,
      appear: styles.appear,
      appearActive: styles.appearActive,
    }}
    transitionEnterTimeout={1000}
    transitionLeaveTimeout={1000}
    transitionAppear
    transitionAppearTimeout={1000}
  >
    {type ?
      <div>
        <Alert
          type={type}
          message={message}
          clearAlert={clearAlert}
        />
      </div> :
      null
    }
  </CSSTransitionGroup>
);

Alerts.propTypes = {
  type: PropTypes.string,
  message: PropTypes.string,
  clearAlert: PropTypes.func.isRequired,
};

export default Alerts;
