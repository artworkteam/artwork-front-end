import React from 'react';
import PropTypes from 'prop-types';
import { Field } from 'redux-form';
import classNames from 'classnames';
import Errors from '../../constants/Errors';
import styles from './ErrorField.css';

const ErrorMessages = ({ className, meta: { touched, error } }) => {
  if (!error) return null;
  const { code, args = [] } = error[0];
  const resolver = Errors[code] || code;
  const message = typeof resolver === 'function' ? resolver(...args) : resolver;
  if (touched && message) {
    return (
      <p className={classNames(styles.errors, className)}>
        {message}
      </p>
    );
  }
  return null;
};

ErrorMessages.propTypes = {
  className: PropTypes.string,
  meta: PropTypes.shape({
    error: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.array,
    ]),
  }).isRequired,
};

const ErrorField = ({ name, ...props }) => (
  <Field
    name={name}
    component={ErrorMessages}
    {...props}
  />
);
export default ErrorField;

ErrorField.propTypes = {
  name: PropTypes.string.isRequired,
};
