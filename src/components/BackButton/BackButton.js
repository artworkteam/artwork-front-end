import React from 'react';
import { browserHistory } from 'react-router';
import { Button } from '../../components';
import { LeftArrowIcon } from '../Icons/Icons';
import styles from './backButton.css';


const BackButton = () => (
  <Button
    styleType='clear'
    className={styles.backButton}
    onClick={() => browserHistory.push('/')}
  >
    <LeftArrowIcon />
    Назад
  </Button>
);

export default BackButton;
