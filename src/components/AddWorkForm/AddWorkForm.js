import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { reduxForm, Field, formValueSelector } from 'redux-form';
import { connect } from 'react-redux';
import { UploadcareWidget } from '../../components';
import validation, { valid, notEmpty, size } from '../../utils/validation';
import TextAreaField from '../TextAreaField/TextAreaField';
import ErrorField from '../ErrorField/ErrorField';
import Form from '../Form/Form';
import styles from './addWorkForm.css';

const selector = formValueSelector('addWork');

@reduxForm({
  form: 'addWork',
  validate: validation({
    description: valid(notEmpty(), size(0, 1000)),
    imageId: valid(notEmpty()),
  }),
})
@connect((store) => {
  const imageId = selector(store, 'imageId');

  return {
    imageId,
  };
})

export default class AddWorkForm extends Component {
  static propTypes = {
    change: PropTypes.func.isRequired,
    invalid: PropTypes.bool.isRequired,
  }

  state = {
    widgetPlaceholderRef: null,
  }

  imageChange = (uuid) => {
    const { change } = this.props;

    change('imageId', uuid);
  }

  componentDidMount() {
    this.setState({
      widgetPlaceholderRef: this.widgetPlaceholderRef,
    });
  }

  render() {
    const { invalid, imageId } = this.props;
    const { widgetPlaceholderRef } = this.state;

    return (
      <Form className={styles.form} {...this.props}>
        <div className={styles.description}>
          <label className={styles.labelDescription} htmlFor='description'>Введите описание работы</label>
          <TextAreaField
            className={styles.inputDescription}
            name='description'
            autoFocus
            minRows={4}
            maxRows={4}
          />
          <ErrorField name='description' />
        </div>
        <div className={styles.panel}>
          <UploadcareWidget
            imageChange={this.imageChange}
            widgetPlaceholderRef={widgetPlaceholderRef}
            imageId={imageId}
          />
        </div>
        <Field name='imageId' component='input' type='hidden' />
        <div className={styles.buttons}>
          <button type='submit' className={styles.submitButton} disabled={invalid}>Выложить работу</button>
          <div className={styles.widget}>
            <div ref={(ref) => { this.widgetPlaceholderRef = ref; }} />
          </div>
        </div>
      </Form>
    );
  }
}
