import React from 'react';
import PropTypes from 'prop-types';
import BodyClassName from 'react-body-classname';
import { Link } from 'react-router';
import { Logo, Button } from '../../components';
import errorImg from './error.svg';
import styles from './notFound.css';

const NotFound = () => (
  <BodyClassName className='noScroll'>
    <div className={styles.notFound}>
      <div className={styles.logo}>
        <Logo />
      </div>
      <div className={styles.content}>
        <img className={styles.errorImg} src={errorImg} alt='' />
        <span className={styles.titleTop}>Ой!</span>
        <h1 className={styles.titleBottom}>Страница не найдена</h1>
        <p className={styles.text}>
          Скорее всего, это случилось по одной из следующих причин: страница переехала, страницы больше нет или вам просто нравится изучать 404 страницы
        </p>
        <Link to='/'>
          <Button
            styleType='basic'
            className={styles.backButton}
          >
            Вернуться на главную
          </Button>
        </Link>
      </div>
    </div>
  </BodyClassName>
);

export default NotFound;
