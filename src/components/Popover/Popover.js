import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import styles from './popover.css';


export default class Popover extends Component {
  static propTypes = {
    className: PropTypes.string,
    children: PropTypes.node.isRequired,
    isOpen: PropTypes.bool.isRequired,
    onOuterAction: PropTypes.func.isRequired,
  };

  constructor(props) {
    super(props);
    if (props.children.length !== 2) {
      throw new Error('Popover component requires exactly 2 children');
    }
  }

  componentDidMount() {
    document.addEventListener('mousedown', this.onOutsideClick);
  }

  componentWillUnmount() {
    document.removeEventListener('mousedown', this.onOutsideClick);
  }

  onOutsideClick = (e) => {
    if (!this.props.isOpen) {
      return;
    }

    e.stopPropagation();
    const localNode = this.popoverRef;
    let source = e.target;

    while (source.parentNode) {
      if (source === localNode) {
        return;
      }
      source = source.parentNode;
    }

    this.props.onOuterAction();
  }

  render() {
    const { className, children, isOpen } = this.props;

    return (
      <div
        className={classNames(className, styles.popover)}
        ref={(ref) => { this.popoverRef = ref; }}
      >
        { children[0] }
        { isOpen && children[1] }
      </div>
    );
  }
}
