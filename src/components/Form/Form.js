import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { handleSubmitError } from '../../utils/forms';
import LoaderSpinner from '../LoaderSpinner/LoaderSpinner';
import styles from './Form.css';

export default class Form extends Component {
  static propTypes = {
    className: PropTypes.string,
    handleSubmit: PropTypes.func.isRequired,
    onSubmit: PropTypes.func.isRequired,
    submitting: PropTypes.bool,
    children: PropTypes.node.isRequired,
  };

  onSubmit = (e) => {
    e.preventDefault();
    const { handleSubmit, onSubmit } = this.props;
    const submit = (...args) => handleSubmitError(onSubmit(...args));
    handleSubmit(submit)(e);
  };

  render() {
    const { className, submitting, children } = this.props;
    return (
      <form className={styles.form} onSubmit={this.onSubmit}>
        {submitting && <div className={styles.loader}><LoaderSpinner /></div>}
        <div className={classNames(className, styles.wrapper, submitting && styles.hide)}>
          {children}
        </div>
      </form>
    );
  }
}
