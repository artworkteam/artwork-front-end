import React from 'react';
import PropTypes from 'prop-types';
import { reduxForm } from 'redux-form';
import { Button, Form } from '../../components/';
import styles from './deleteWorkForm.css';

const DeleteWorkForm = ({ closeModal, ...props }) => (
  <Form className={styles.deleteWorkForm} {...props}>
    <Button styleType='basic' type='submit'>Удалить</Button>
    <Button styleType='basicWhite' onClick={closeModal}>Отмена</Button>
  </Form>
);

DeleteWorkForm.propTypes = {
  closeModal: PropTypes.func.isRequired,
};

export default reduxForm({
  form: 'deleteWork',
})(DeleteWorkForm);
