import React from 'react';
import styles from './footer.css';

const Footer = () => (
  <footer className={styles.footer}>
    <div className={styles.wrapper}>
      <div className={styles.text}>
        <strong className={styles.copyright}>ARTWORK © 2017.</strong>
        Все работы принадлежат их непосредственным владельцам.
      </div>
    </div>
  </footer>
);

export default Footer;
