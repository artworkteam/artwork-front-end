import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { Button, Popover, Username } from '../../components';
import Avatar from '../Avatar/Avatar';
import { AlarmIcon } from '../Icons/Icons';
import formatDateTime from '../../utils/formatDate';
import styles from './NotificationButton.css';

const getTypeText = ({ type }) => {
  switch (type) {
    case 'WORK_COMMENTED':
      return 'Добавил(а) комментарий к вашей работе';
    case 'SUGGESTION_CREATED':
      return 'Оставил(а) отметку на вашей работе';
    case 'WORK_LIKED':
      return 'Оценил(а) вашу работу';
    case 'COMMENT_LIKED':
      return 'Оценил(а) ваш коментарий';
    default:
      return '';
  }
};

const Notification = ({ n, onClick }) => (
  <div className={styles.notification}>
    <div
      className={styles.notificationContainer}
      onClick={onClick}
    >
      <Avatar
        className={styles.author}
        user={n.initiator}
        onClick={e => e.stopPropagation()}
      />
      <div className={styles.body}>
        <div className={styles.head}>
          <Username
            user={n.initiator}
            styleType={'white'}
            onClick={e => e.stopPropagation()}
          />
          <span className={styles.time}>{formatDateTime(n.createdDate)}</span>
        </div>
        <div className={styles.type}>{getTypeText(n)}</div>
      </div>
    </div>
    <hr />
  </div>
);

Notification.propTypes = {
  n: PropTypes.shape({}).isRequired,
  onClick: PropTypes.func,
};

const NotificationsList = ({ items, notifications, onShowAllClick, onNotificationClick }) => (
  <div className={styles.listWrapper}>
    <div className={styles.listHead}>
      <span>УВЕДОМЛЕНИЯ</span>
      <div className={styles.count}>
        <span className={styles.countText}>
          {items.length}
        </span>
      </div>
    </div>
    <div className={styles.list}>
      {items.map(id =>
        <Notification key={id} n={notifications[id]} onClick={() => onNotificationClick(id)} />
      )}
      <Button
        styleType='clear'
        className={styles.listButton}
        onClick={onShowAllClick}
      >
        Посмотреть все
      </Button>
    </div>
  </div>
);

NotificationsList.propTypes = {
  notifications: PropTypes.object.isRequired,
  items: PropTypes.arrayOf(PropTypes.string).isRequired,
  onShowAllClick: PropTypes.func.isRequired,
  onNotificationClick: PropTypes.func.isRequired,
};

export default class NotificationButton extends Component {
  static propTypes = {
    notifications: PropTypes.object.isRequired,
    items: PropTypes.arrayOf(PropTypes.string).isRequired,
    unread: PropTypes.bool,
    onShowAll: PropTypes.func.isRequired,
    onShowNotification: PropTypes.func.isRequired,
    onListHidden: PropTypes.func.isRequired,
  };

  state = {
    showList: false,
  };

  onToggleList = () => this.setState({ showList: !this.state.showList });
  hideList = () => {
    this.props.onListHidden();
    this.setState({ showList: false });
  };

  onShowAllClick = () => {
    this.hideList();
    this.props.onShowAll();
  };

  onNotificationClick = (id) => {
    this.hideList();
    this.props.onShowNotification(id);
  };

  render() {
    const { notifications, items, unread } = this.props;
    const { showList } = this.state;
    return (
      <span className={styles.button}>
        <Popover
          isOpen={showList}
          onOuterAction={this.hideList}
        >
          <Button
            styleType='clear'
            className={classNames(styles.icon, unread && styles.alarm)}
            onClick={this.onToggleList}
          >
            <AlarmIcon />
          </Button>
          <NotificationsList
            notifications={notifications}
            items={items}
            onShowAllClick={this.onShowAllClick}
            onNotificationClick={this.onNotificationClick}
          />
        </Popover>
      </span>
    );
  }
}
