import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Waypoint from 'react-waypoint';

export default class extends Component {
  static propTypes = {
    className: PropTypes.string,

    items: PropTypes.arrayOf(PropTypes.any).isRequired,
    renderItem: PropTypes.func.isRequired,
    renderLoading: PropTypes.func.isRequired,
    renderPlaceholder: PropTypes.func.isRequired,

    hasMore: PropTypes.bool.isRequired,
    loading: PropTypes.bool.isRequired,
    loadMore: PropTypes.func.isRequired,
  };

  onEnter = () => {
    if (!this.props.loading) {
      this.props.loadMore();
    }
  };

  render() {
    const { className, items, renderItem,
      hasMore, loading, renderLoading, renderPlaceholder } = this.props;
    return (
      <div className={className}>
        {items.map(renderItem)}
        {hasMore &&
        <Waypoint
          key={items.length}
          onEnter={this.onEnter}
          scrollableAncestor={window}
        />
        }
        {loading && renderLoading()}
        {!hasMore && !loading && items.length === 0 && renderPlaceholder()}
      </div>
    );
  }
}
