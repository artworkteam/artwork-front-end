import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import styles from './LoaderSpinner.css';

const LoaderSpinner = ({ className }) => (
  <div className={classNames(styles.loaderSpinner, className)} />
);

export default LoaderSpinner;

LoaderSpinner.propTypes = {
  className: PropTypes.string,
};
