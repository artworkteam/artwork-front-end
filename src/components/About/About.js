import React from 'react';
import PropTypes from 'prop-types';
import supportsWebp from 'supports-webp';
import classNames from 'classnames';
import { Content } from '../../components';
import telegramIcon from './telegram.svg';
import slackIcon from './slack.svg';
import styles from './about.css';


const About = () => (
  <Content>
    <div className={styles.about}>
      <div className={classNames(styles.header, { [styles.headerWebp]: supportsWebp })}>
        <h1 className={styles.companyName}>Artwork</h1>
        <h2 className={styles.subtitle}>Проверь на прочность свой концепт</h2>
      </div>
      <div className={styles.info}>
        <div className={styles.text}>
          <p className={styles.paragraph}>
              Считается, что успешный дизайн включает в себя множество точек зрения, и критика является отличным способом, чтобы собрать эти точки зрения.
            </p>
          <p className={styles.paragraph}>
              Мы знаем, что без команды, которая может покритиковать твою работу, может быть очень сложно «двигаться в темноте». Именно поэтому мы создали для тебя такое креативное сообщество.
            </p>
          <p className={styles.paragraph}>
            <strong>Artwork</strong> позволяет тебе делиться частью проекта с профессиональным сообществом / опытными дизайнерами перед тем, как ты покажешь его клиенту. Это шанс провести быстрое черновое тестирование, а также способ оценить реакцию с минимальным риском.
            </p>
          <p className={styles.paragraphLast}>
              Ты просто описываешь задачу и прикрепляешь изображение, результат не заставит себя долго ждать!
            </p>
          <hr className={styles.separator} />
        </div>
      </div>
      <div className={styles.footer}>
        <p className={styles.footerText}>Мы делаем этот сервис для тебя и нам важно знать твое мнение.</p>
        <div className={styles.icons}>
          <a
            className={styles.telegramLink}
            href='https://t.me/artworkapp'
            target='_blank'
            rel='noreferrer noopener'
          >
            <img src={telegramIcon} className={styles.telegramIcon} />
          </a>
          <a
            className={styles.slackLink}
            href='http://slack.artwork.pro'
            target='_blank'
            rel='noreferrer noopener'
          >
            <img src={slackIcon} className={styles.slackIcon} />
          </a>
        </div>
      </div>
    </div>
  </Content>
);


export default About;
