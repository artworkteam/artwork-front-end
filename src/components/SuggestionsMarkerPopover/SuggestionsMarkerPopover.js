import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { selectSuggestion, hidePreview } from '../../actions/SuggestionsActions';
import { DateTime, SuggestionCommentForm, Button, Username } from '../../components';
import styles from './suggestionsMarkerPopover.css';


export default class SuggestionsMarkerPopover extends Component {
  static propTypes = {
    dispatch: PropTypes.func.isRequired,
    suggestion: PropTypes.shape({}),
    workId: PropTypes.string.isRequired,
    x: PropTypes.number.isRequired,
    y: PropTypes.number.isRequired,
    imageContainerWidth: PropTypes.number.isRequired,
    imageContainerHeight: PropTypes.number.isRequired,
  };

  constructor(props) {
    super(props);

    //popover shadow size in px for right calculate
    this.shadowSize = 15;

    this.state = {
      positionClassName: styles.windowRight,
      visible: false,
    };
  }

  componentDidMount() {
    /* eslint-disable */
    this.setState({
      visible: true,
      positionClassName: this.getPosition(),
    });
    /* eslint-enable */
  }

  componentWillReceiveProps(nextProps) {
    const { imageContainerWidth, imageContainerHeight, x, y } = this.props;
    const {
      imageContainerWidth: nextImageContainerWidth,
      imageContainerHeight: nextImageContainerHeight,
      x: nextX,
      y: nextY,
    } = nextProps;

    const isContainerSizeChanged = (
      imageContainerWidth !== nextImageContainerWidth ||
      imageContainerHeight !== nextImageContainerHeight
    );
    const isPositionChanged = (x !== nextX || y !== nextY);

    if (isContainerSizeChanged || isPositionChanged) {
      this.setState({
        visible: false,
        positionClassName: styles.windowRight,
      });
    }
  }

  componentDidUpdate() {
    const { visible } = this.state;

    if (!visible) {
      /* eslint-disable */
      this.setState({
        visible: true,
        positionClassName: this.getPosition(),
      });
      /* eslint-enable */
    }
  }


  getPosition = () => {
    const width = this.props.imageContainerWidth;
    const height = this.props.imageContainerHeight;
    //getting additional params that affect element size
    const shadowSize = this.shadowSize;
    const popoverWidth = this.popoverRef.clientWidth;
    //getting coordinates of popover element
    const coordinates = this.popoverRef.getBoundingClientRect();

    /*we cant get over left in default displaying on right side,
    but we need make sure that elem will not over when we render him
    on bottom side*/
    const isOverLeft = coordinates.left - shadowSize - (popoverWidth / 2) < 1;
    const isOverRight = coordinates.right + shadowSize > width;
    const isOverTop = coordinates.top - shadowSize < 1;
    const isOverBottom = coordinates.bottom + shadowSize > height;

    if (isOverTop) {
      if (isOverRight) {
        return styles.windowBottomLeft;
      }
      if (isOverLeft) {
        return styles.windowBottomRight;
      }

      return styles.windowBottom;
    }
    if (isOverBottom) {
      if (isOverRight) {
        return styles.windowTopLeft;
      }
      if (isOverLeft) {
        return styles.windowTopRight;
      }

      return styles.windowTop;
    }
    if (isOverRight) {
      return styles.windowLeft;
    }

    return styles.windowRight;
  }

  closeClick = () => {
    const { dispatch } = this.props;

    dispatch(selectSuggestion(null));
    dispatch(hidePreview());
  }

  render() {
    const { workId, suggestion, x, y } = this.props;
    const { positionClassName, visible } = this.state;

    return (
      <div
        className={classNames(
          positionClassName,
          { [styles.windowHidden]: !visible },
        )}
        style={{
          left: `${x}%`,
          top: `${y}%`,
        }}
        ref={(ref) => { this.popoverRef = ref; }}
      >
        <div className={styles.closeButton}>
          <Button
            styleType='close'
            onClick={this.closeClick}
          />
        </div>
        { suggestion ?
          <div className={styles.windowContent}>
            <div className={styles.comment}>
              <p className={styles.commentText}>
                {suggestion.description}
              </p>
              <div className={styles.commentInfo}>
                <div className={suggestion.isMine ? styles.commentNameMyOwn : styles.commentName}>
                  { suggestion.isMine
                    ? 'Я'
                    : <Username user={suggestion.author} styleType={'noUppercase'} link={false} />
                  }
                </div>
                <DateTime className={styles.commentDate} timestamp={suggestion.createdDate} />
              </div>
            </div>
          </div>
        :
          <div className={styles.windowForm}>
            <SuggestionCommentForm workId={workId} x={x} y={y} />
          </div>
      }
      </div>
    );
  }
}
