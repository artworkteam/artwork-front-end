import React from 'react';
import PropTypes from 'prop-types';


export const AlarmIcon = (props) => {
  const width = props.width || '15';
  const height = props.height || '15';
  const fillColor = props.fillColor || 'white';

  return (
    <svg
      xmlns='http://www.w3.org/2000/svg'
      width={width} height={height}
      fill={fillColor}
      viewBox={'0 0 30 32'}
    >
      <path d='M26.33 20.467v-7.362c0-5.182-3.692-9.51-8.645-10.647C17.655 1.096 16.52 0 15.115 0c-1.402 0-2.54 1.097-2.57 2.458C7.59 3.593 3.897 7.923 3.897 13.104v7.36c-1.864 0-3.375 1.476-3.375 3.296v2.323h29.18V23.76c.002-1.82-1.508-3.293-3.372-3.293zM15.115 31.502c2.24 0 4.11-1.47 4.702-3.47H10.41c.596 2 2.464 3.47 4.704 3.47z' />
    </svg>
  );
};

AlarmIcon.propTypes = {
  width: PropTypes.string,
  height: PropTypes.string,
  fillColor: PropTypes.string,
};

export const TurnOffIcon = (props) => {
  const width = props.width || '15';
  const height = props.height || '15';
  const fillColor = props.fillColor || 'white';

  return (
    <svg
      xmlns='http://www.w3.org/2000/svg'
      width={width} height={height}
      fill={fillColor}
      viewBox={'0 0 27 32'}
    >
      <path d='M13.677 30.282c-3.556 0-6.898-1.405-9.414-3.954-2.513-2.552-3.9-5.943-3.9-9.552s1.387-7 3.9-9.552c.8-.812 2.095-.812 2.896 0s.8 2.126 0 2.938c-1.742 1.768-2.7 4.116-2.7 6.614s.958 4.848 2.7 6.614c1.74 1.766 4.052 2.737 6.516 2.737s4.776-.972 6.517-2.737c1.743-1.766 2.7-4.114 2.7-6.614s-.96-4.848-2.698-6.614c-.8-.812-.8-2.126 0-2.938s2.095-.812 2.896 0c2.514 2.554 3.9 5.945 3.9 9.552s-1.386 7-3.9 9.552c-2.514 2.55-5.857 3.954-9.415 3.954zm0-15.737c-1.133 0-2.048-.93-2.048-2.078V2.077C11.63.93 12.544 0 13.676 0s2.048.93 2.048 2.077v10.39c0 1.147-.913 2.078-2.048 2.078z' />
    </svg>
  );
};

TurnOffIcon.propTypes = {
  width: PropTypes.string,
  height: PropTypes.string,
  fillColor: PropTypes.string,
};

export const ViewIcon = (props) => {
  const width = props.width || '24';
  const height = props.height || '15';
  const fillColor = props.fillColor || '#aebeca';

  return (
    <svg
      xmlns='http://www.w3.org/2000/svg'
      width={width} height={height}
      fill={fillColor}
      viewBox={'0 0 50 32'}
    >
      <path d='M48.72 14.45c-.1-.148-2.502-3.653-6.62-7.17C36.597 2.576 30.61.09 24.785.09S12.973 2.577 7.47 7.28C3.352 10.797.95 14.3.85 14.45L0 15.7l.85 1.253c.1.147 2.502 3.652 6.62 7.17 5.503 4.703 11.49 7.188 17.315 7.188s11.812-2.484 17.315-7.186c4.118-3.52 6.52-7.023 6.62-7.17l.852-1.253-.852-1.25zM24.786 26.636c-5.184 0-9.402-4.906-9.402-10.936 0-.944.104-1.86.298-2.736l-2.147-.42c-.21 1.013-.322 2.07-.322 3.157 0 3.896 1.43 7.41 3.712 9.87-2.937-1.262-5.434-3.032-7.32-4.633-2.334-1.984-4.084-3.99-5.08-5.235.997-1.246 2.747-3.252 5.08-5.235 1.886-1.6 4.383-3.372 7.32-4.633l1.44 1.888c1.682-1.832 3.94-2.955 6.42-2.955 5.185 0 9.403 4.906 9.403 10.936S29.97 26.64 24.786 26.64zm15.18-5.7c-1.886 1.6-4.383 3.372-7.32 4.633 2.28-2.46 3.712-5.973 3.712-9.87s-1.43-7.408-3.712-9.868c2.937 1.262 5.434 3.032 7.32 4.633 2.334 1.984 4.084 3.99 5.082 5.235-.998 1.246-2.748 3.252-5.082 5.236zm-21.163-7.362c-.176.673-.272 1.386-.272 2.126 0 4.02 2.802 7.277 6.256 7.277S31.04 19.72 31.04 15.7s-2.8-7.275-6.254-7.275c-1.683 0-3.21.774-4.333 2.03l3.085 4.046-4.735-.926z' />
    </svg>
  );
};

ViewIcon.propTypes = {
  width: PropTypes.string,
  height: PropTypes.string,
  fillColor: PropTypes.string,
};

export const DeleteIcon = (props) => {
  const width = props.width || '16';
  const height = props.height || '20';
  const fillColor = props.fillColor || 'white';

  return (
    <svg
      xmlns='http://www.w3.org/2000/svg'
      width={width} height={height}
      fill={fillColor}
      viewBox={'0 0 25 32'}
    >
      <path d='M22.852 3.736H18.63V.983c0-.554-.447-.983-1-.983-.053 0-.088.018-.106.036C17.506.018 17.47 0 17.452 0H7.382c-.55 0-.98.43-.98.983v2.753h-4.24C.95 3.736.006 4.683.006 5.9v3.54h1.87v20.147c0 1.216.928 2.145 2.14 2.145H21c1.21 0 2.155-.93 2.155-2.145V9.44h1.853V5.9c0-1.217-.945-2.164-2.156-2.164zm-14.49-1.77h8.288v1.77H8.363v-1.77zm12.815 27.62c0 .126-.054.18-.178.18H4.015c-.125 0-.178-.053-.178-.18V9.44h17.34v20.147zM23.03 7.474H1.983V5.9c0-.125.054-.197.178-.197h20.69c.126 0 .18.072.18.197v1.573zm-6.932 4.273h1.978v16.268h-1.978V11.746zm-4.562 0h1.978v16.268h-1.978V11.746zm-4.563 0H8.95v16.268H6.974V11.746z' />
    </svg>
  );
};

DeleteIcon.propTypes = {
  width: PropTypes.string,
  height: PropTypes.string,
  fillColor: PropTypes.string,
};

export const ChatIcon = (props) => {
  const width = props.width || '18';
  const height = props.height || '18';
  const fillColor = props.fillColor || '#aebeca';

  return (
    <svg
      xmlns='http://www.w3.org/2000/svg'
      width={width} height={height}
      fill={fillColor}
      viewBox={'0 0 60 60'}
    >
      <path d='M30 1.5c-16.542 0-30 12.112-30 27 0 5.204 1.646 10.245 4.768 14.604-.59 6.537-2.175 11.39-4.475 13.69-.304.303-.38.768-.188 1.152.17.343.52.554.895.554.046 0 .092-.003.14-.01.404-.057 9.812-1.41 16.617-5.34C21.62 54.71 25.737 55.5 30 55.5c16.542 0 30-12.112 30-27s-13.458-27-30-27zm-14 31c-2.206 0-4-1.794-4-4s1.794-4 4-4 4 1.794 4 4-1.794 4-4 4zm14 0c-2.206 0-4-1.794-4-4s1.794-4 4-4 4 1.794 4 4-1.794 4-4 4zm14 0c-2.206 0-4-1.794-4-4s1.794-4 4-4 4 1.794 4 4-1.794 4-4 4z' />
    </svg>
  );
};

ChatIcon.propTypes = {
  width: PropTypes.string,
  height: PropTypes.string,
  fillColor: PropTypes.string,
};

export const Chat2Icon = (props) => {
  const width = props.width || '70';
  const height = props.height || '65';
  const fillColor = props.fillColor || '#fff';

  return (
    <svg
      xmlns='http://www.w3.org/2000/svg'
      width={width} height={height}
      fill={fillColor}
      viewBox={'0 0 70 65'}
    >
      <g fill={fillColor} fillRule='evenodd'><path d='M63.084.624L23.252.542a6.926 6.926 0 0 0-6.92 6.92v5.977l-9.414.02A6.926 6.926 0 0 0 0 20.376v24.665c0 3.814 3.103 6.918 6.917 6.918H14v11.666a1.167 1.167 0 0 0 2.03.786l11.32-12.455 19.398-.08a6.926 6.926 0 0 0 6.92-6.917v-.132l6.135 6.75a1.164 1.164 0 0 0 1.284.304c.45-.173.746-.607.746-1.09V39.128h1.25A6.926 6.926 0 0 0 70 32.207V7.544a6.925 6.925 0 0 0-6.916-6.92zm-11.75 44.334a4.59 4.59 0 0 1-4.59 4.584l-19.915.082c-.33 0-.64.14-.86.382l-9.637 10.6V50.79c0-.644-.522-1.166-1.166-1.166h-8.25a4.59 4.59 0 0 1-4.584-4.585V20.375A4.59 4.59 0 0 1 6.92 15.79l10.58-.02h.002l29.015-.06h.23a4.59 4.59 0 0 1 4.586 4.584v24.663zm16.333-12.752a4.59 4.59 0 0 1-4.585 4.585h-2.415c-.645 0-1.167.522-1.167 1.168v9.815l-5.833-6.417V20.293c0-3.815-3.104-6.917-6.92-6.917h-.12l-27.96.057V7.46a4.59 4.59 0 0 1 4.582-4.585l39.83.08h.003a4.59 4.59 0 0 1 4.585 4.586v24.666z'/><path d='M14 28.524a4.67 4.67 0 0 0-4.667 4.666A4.67 4.67 0 0 0 14 37.858a4.67 4.67 0 0 0 4.667-4.666A4.67 4.67 0 0 0 14 28.522zm0 7a2.336 2.336 0 0 1-2.333-2.334A2.336 2.336 0 0 1 14 30.858a2.336 2.336 0 0 1 2.333 2.334A2.336 2.336 0 0 1 14 35.522zm12.833-7a4.67 4.67 0 0 0-4.666 4.666 4.67 4.67 0 0 0 4.666 4.667A4.67 4.67 0 0 0 31.5 33.19a4.67 4.67 0 0 0-4.667-4.666zm0 7A2.336 2.336 0 0 1 24.5 33.19a2.336 2.336 0 0 1 2.333-2.333 2.336 2.336 0 0 1 2.334 2.334 2.336 2.336 0 0 1-2.334 2.334zm12.834-7A4.67 4.67 0 0 0 35 33.19a4.67 4.67 0 0 0 4.667 4.667 4.67 4.67 0 0 0 4.666-4.666 4.67 4.67 0 0 0-4.666-4.666zm0 7a2.336 2.336 0 0 1-2.334-2.334 2.336 2.336 0 0 1 2.334-2.333A2.336 2.336 0 0 1 42 33.19a2.336 2.336 0 0 1-2.333 2.334z'/></g>
    </svg>
  );
};

Chat2Icon.propTypes = {
  width: PropTypes.string,
  height: PropTypes.string,
  fillColor: PropTypes.string,
};

export const PlaceholderIcon = (props) => {
  const width = props.width || '18';
  const height = props.height || '18';
  const fillColor = props.fillColor || '#aebeca';

  return (
    <svg
      xmlns='http://www.w3.org/2000/svg'
      width={width} height={height}
      fill={fillColor}
      viewBox={'0 0 434.174 434.174'}
    >
      <path d='M217.087 119.397c-24.813 0-45 20.187-45 45s20.187 45 45 45 45-20.187 45-45-20.186-45-45-45z' /><path d='M217.087 0c-91.874 0-166.62 74.745-166.62 166.62 0 38.93 13.42 74.78 35.878 103.176l130.742 164.378L347.83 269.796c22.456-28.396 35.877-64.247 35.877-103.177C383.707 74.744 308.96 0 217.087 0zm0 239.397c-41.355 0-75-33.645-75-75s33.645-75 75-75 75 33.645 75 75-33.644 75-75 75z' />
    </svg>
  );
};

PlaceholderIcon.propTypes = {
  width: PropTypes.string,
  height: PropTypes.string,
  fillColor: PropTypes.string,
};


export const UploadIcon = (props) => {
  const width = props.width || '34';
  const height = props.height || '32';
  const fillColor = props.fillColor || '#139edc';

  return (
    <svg
      xmlns='http://www.w3.org/2000/svg'
      width={width} height={height}
      fill={fillColor}
      viewBox={'0 0 34 32'}
    >
      <path d='M14.667 16.39h4.19V8.193h6.286L16.763 0 8.38 8.193h6.287v8.195zm6.285-4.61v3.16l9.594 3.498-13.785 5.026-13.784-5.026 9.594-3.498v-3.16L0 16.39v8.195l16.76 6.146 16.763-6.145V16.39l-12.57-4.61z' />
    </svg>
  );
};

UploadIcon.propTypes = {
  width: PropTypes.string,
  height: PropTypes.string,
  fillColor: PropTypes.string,
};

export const PencilIcon = (props) => {
  const width = props.width || '20';
  const height = props.height || '20';
  const fillColor = props.fillColor || 'white';

  return (
    <svg
      xmlns='http://www.w3.org/2000/svg'
      width={width} height={height}
      fill={fillColor}
      viewBox={'0 0 32 32'}
    >
      <path d='M30.197 1.663C29.127.59 27.7 0 26.185 0s-2.94.59-4.01 1.66L5.11 18.627c-.1.1-.178.22-.226.354L1.022 29.6c-.125.343-.044.73.208.995.185.195.44.3.7.3.096 0 .19-.014.284-.042l9.606-2.947c.15-.045.288-.128.4-.24L30.2 9.69c1.073-1.07 1.663-2.498 1.663-4.014s-.59-2.94-1.665-4.013zM10.71 26.228l-3.597 1.105c-.21-.615-.536-1.16-.98-1.605-.388-.385-.837-.685-1.31-.928l1.645-4.524h2.22v1.93c0 .535.433.966.967.966h1.704l-.65 3.056zm14.21-13.994L13.158 23.998l.338-1.59c.06-.286-.012-.582-.195-.808s-.46-.358-.75-.358h-1.93v-1.93c0-.534-.433-.966-.966-.966H8.13L19.613 6.93l.012.01c.707-.708 1.648-1.097 2.647-1.097s1.942.39 2.647 1.097c.705.708 1.098 1.648 1.098 2.648s-.39 1.94-1.098 2.647zm3.912-3.91l-.9.9c-.087-1.38-.66-2.665-1.644-3.65s-2.273-1.56-3.656-1.645l.908-.902c.705-.708 1.646-1.097 2.646-1.097s1.94.39 2.646 1.098c.71.708 1.1 1.648 1.1 2.647 0 1-.39 1.942-1.1 2.648zm-6.343-.317l-9.656 9.655c-.377.378-.377.988 0 1.365.187.188.435.283.683.283s.493-.095.683-.283l9.655-9.655c.378-.377.378-.988 0-1.365s-.988-.378-1.365 0z' />
    </svg>
  );
};

PencilIcon.propTypes = {
  width: PropTypes.string,
  height: PropTypes.string,
  fillColor: PropTypes.string,
};

export const VkontakteIcon = (props) => {
  const width = props.width || '22';
  const height = props.height || '20';
  const fillColor = props.fillColor || 'white';

  return (
    <svg
      xmlns='http://www.w3.org/2000/svg'
      width={width} height={height}
      fill={fillColor}
      viewBox={'0 0 34 32'}
    >
      <path d='M34.24 9.28q.416 1.152-2.688 5.248-.416.576-1.152 1.536-.704.896-.992 1.28t-.544.864-.224.768.256.608.576.768 1.024.96q.064.032.064.064 2.528 2.336 3.424 3.936.064.096.128.224t.128.48-.032.608-.448.48-1.056.224l-4.544.096q-.448.064-1.024-.096t-.928-.416l-.352-.192q-.544-.384-1.248-1.152t-1.216-1.376-1.088-1.056-1.024-.256q-.032 0-.128.064t-.32.256-.384.512-.288.928-.128 1.408q0 .256-.064.48t-.128.32l-.064.096q-.32.352-.96.384h-2.048q-1.28.096-2.592-.288t-2.368-.928-1.824-1.184-1.28-1.024l-.448-.448q-.16-.16-.48-.544t-1.28-1.6-1.888-2.72-2.176-3.744T.096 9.984q-.128-.288-.128-.48t.064-.288l.064-.096q.288-.352 1.024-.352l4.896-.032q.224.032.416.096t.288.16l.096.064q.288.192.416.576.352.896.832 1.824t.736 1.472l.288.512q.512 1.088.992 1.856t.864 1.216.736.704.608.256.48-.096q.032-.032.096-.096t.224-.384.224-.832.16-1.472 0-2.208q-.032-.736-.16-1.312t-.224-.832l-.128-.192q-.448-.608-1.504-.768-.256-.064.096-.448.256-.32.672-.544.928-.448 4.256-.416 1.472.032 2.4.224.384.096.608.256t.384.416.16.576.064.8 0 .992-.064 1.28 0 1.472q0 .192-.032.736t0 .864.064.704.192.704.416.448q.128.032.288.064t.48-.192.672-.608.928-1.216 1.216-1.92q1.056-1.856 1.92-4 .064-.192.16-.32t.192-.192l.096-.064.064-.032.256-.064h.352l5.12-.032q.704-.096 1.152.032t.544.32z' />
    </svg>
  );
};

VkontakteIcon.propTypes = {
  width: PropTypes.string,
  height: PropTypes.string,
  fillColor: PropTypes.string,
};

export const FacebookIcon = (props) => {
  const width = props.width || '12';
  const height = props.height || '20';
  const fillColor = props.fillColor || 'white';

  return (
    <svg
      xmlns='http://www.w3.org/2000/svg'
      width={width} height={height}
      fill={fillColor}
      viewBox={'0 0 18 32'}
    >
      <path d='M17.12.224v4.704h-2.784q-1.536 0-2.08.64t-.544 1.92v3.392h5.248l-.704 5.28h-4.544v13.568H6.24V16.16H1.696v-5.28H6.24V6.976q0-3.328 1.856-5.152T13.056 0q2.624 0 4.064.224z' />
    </svg>
  );
};

FacebookIcon.propTypes = {
  width: PropTypes.string,
  height: PropTypes.string,
  fillColor: PropTypes.string,
};

export const HeartIcon = (props) => {
  const width = props.width || '18';
  const height = props.height || '18';
  const fillColor = props.fillColor || '#aebeca';

  return (
    <svg
      xmlns='http://www.w3.org/2000/svg'
      width={width} height={height}
      fill={fillColor}
      viewBox={'0 0 32 32'}
    >
      <path d='M16 29.728q-.448 0-.8-.32L4.064 18.656q-.16-.16-.48-.48t-.992-1.184-1.216-1.728-.96-2.144T0 10.656Q0 6.72 2.272 4.512t6.272-2.24q1.088 0 2.24.384t2.144 1.056 1.728 1.216T16 6.144q.64-.64 1.344-1.216t1.728-1.216 2.144-1.056 2.24-.384q4 0 6.272 2.24T32 10.656q0 3.936-4.096 8.032L16.8 29.408q-.32.32-.8.32z' />
    </svg>
  );
};

HeartIcon.propTypes = {
  width: PropTypes.string,
  height: PropTypes.string,
  fillColor: PropTypes.string,
};

export const FileImageIcon = (props) => {
  const width = props.width || '22';
  const height = props.height || '24';
  const fillColor = props.fillColor || '#62686d';

  return (
    <svg
      xmlns='http://www.w3.org/2000/svg'
      width={width} height={height}
      fill={fillColor}
      viewBox={'0 0 27 32'}
    >
      <path d='M26.208 6.784q.512.512.864 1.344t.352 1.6v20.544q0 .736-.48 1.216T25.728 32h-24q-.736 0-1.216-.512T0 30.272V1.728Q0 .992.512.512T1.728 0h16q.704 0 1.568.352t1.344.864zm-7.936-4.352v6.72h6.72q-.16-.544-.384-.736l-5.6-5.6q-.192-.224-.736-.384zm6.88 27.296V11.424h-7.424q-.736 0-1.216-.512T16 9.728V2.272H2.272v27.456h22.88zm-2.304-8v5.696H4.576V24L8 20.576l2.272 2.272L17.152 16zM8 18.272q-1.44 0-2.432-.992t-.992-2.432.992-2.432T8 11.424t2.432.992.992 2.432-.992 2.432T8 18.272z' />
    </svg>
  );
};

FileImageIcon.propTypes = {
  width: PropTypes.string,
  height: PropTypes.string,
  fillColor: PropTypes.string,
};

export const OkIcon = (props) => {
  const width = props.width || '20';
  const height = props.height || '20';
  const fillColor = props.fillColor || 'white';

  return (
    <svg
      xmlns='http://www.w3.org/2000/svg'
      width={width} height={height}
      fill={fillColor}
      viewBox={'0 0 305 305'}
    >
      <path d='M152.502 0C68.412 0 0 68.413 0 152.5S68.412 305 152.502 305s152.5-68.41 152.5-152.5S236.592 0 152.502 0zm0 280C82.197 280 25 222.807 25 152.5 25 82.198 82.197 25 152.502 25c70.304 0 127.5 57.197 127.5 127.5 0 70.306-57.196 127.5-127.5 127.5z' /><path d='M218.473 93.97l-90.546 90.547L86.53 143.12c-4.883-4.882-12.797-4.882-17.68 0-4.88 4.88-4.88 12.795 0 17.677l50.238 50.237c2.44 2.44 5.64 3.66 8.84 3.66 3.198 0 6.397-1.22 8.838-3.66l99.385-99.385c4.882-4.883 4.882-12.797 0-17.68-4.88-4.88-12.796-4.88-17.677 0z' />
    </svg>
  );
};

OkIcon.propTypes = {
  width: PropTypes.string,
  height: PropTypes.string,
  fillColor: PropTypes.string,
};

export const CancelIcon = (props) => {
  const width = props.width || '20';
  const height = props.height || '20';
  const fillColor = props.fillColor || 'white';

  return (
    <svg
      xmlns='http://www.w3.org/2000/svg'
      width={width} height={height}
      fill={fillColor}
      viewBox={'0 0 305 305'}
    >
      <path d='M152.502 0C68.412 0 0 68.413 0 152.5S68.412 305 152.502 305s152.5-68.41 152.5-152.5S236.592 0 152.502 0zm0 280C82.197 280 25 222.807 25 152.5 25 82.198 82.197 25 152.502 25c70.304 0 127.5 57.197 127.5 127.5 0 70.306-57.196 127.5-127.5 127.5z' /><path d='M170.18 152.5l43.13-43.13c4.882-4.88 4.882-12.795 0-17.677-4.88-4.882-12.796-4.88-17.678 0l-43.13 43.13-43.13-43.13c-4.883-4.882-12.797-4.882-17.68 0-4.88 4.88-4.88 12.795 0 17.677l43.13 43.13-43.13 43.13c-4.88 4.883-4.88 12.797 0 17.68 2.44 2.44 5.64 3.66 8.84 3.66 3.198 0 6.397-1.22 8.838-3.66l43.13-43.132 43.132 43.132c2.44 2.44 5.64 3.66 8.84 3.66s6.397-1.22 8.838-3.66c4.882-4.883 4.882-12.797 0-17.68l-43.13-43.13z' />
    </svg>
  );
};

CancelIcon.propTypes = {
  width: PropTypes.string,
  height: PropTypes.string,
  fillColor: PropTypes.string,
};

export const MegaphoneIcon = (props) => {
  const width = props.width || '32';
  const height = props.height || '32';
  const fillColor = props.fillColor || 'white';

  return (
    <svg
      xmlns='http://www.w3.org/2000/svg'
      width={width} height={height}
      fill={fillColor}
      viewBox={'0 0 32 32'}
    >
      <path d='M25 0c-2.053 0-3.666 1.41-4.824 3.586l-.02-.012C18.472 6.867 15.968 9 13.225 9H4c-2.243 0-4 2.197-4 5 0 2.805 1.757 5 4 5 1.103.004 1.995.896 1.995 2v9c0 1.105.896 2 2 2h4c1.104 0 2-.895 2-2v-1c0-1-1-1.447-1-2v-7c0-.023.016-.04.018-.062.01-.143.05-.275.112-.395.018-.033.037-.06.06-.088.08-.12.18-.217.303-.293.006-.004.008-.01.014-.014.004 0 .007-.004.01-.004.08-.045.177-.055.267-.08 2.523.268 4.808 2.305 6.376 5.373l.025-.012C21.34 26.595 22.95 28 25 28c4.596 0 7-7.043 7-14S29.596 0 25 0zm-5 14c0-1.037.06-2.04.164-3H23c1.104 0 2 1.344 2 3 0 1.658-.896 3-2 3h-2.836c-.103-.96-.164-1.96-.164-3zM2 14c0-1.656.896-3 2-3H11.014c-.62.73-1.014 1.787-1.014 3 0 1.215.394 2.273 1.014 3H4c-1.104 0-2-1.342-2-3zm9.995 16h-4v-9c0-.73-.195-1.41-.537-2h.698v.012h3.008c-.107.31-.17.64-.17.988v7c0 .963.54 1.604.86 1.986.044.053.096.107.14.166V30zm1.23-12.988H13V17c-1.104 0-2-1.342-2-3 0-1.656.896-3 2-3h.226c1.886 0 3.652-.742 5.206-2.018C18.146 10.592 18 12.297 18 14c0 1.71.146 3.42.434 5.03-1.555-1.276-3.322-2.018-5.208-2.018zM25 26c-2.018 0-3.75-2.87-4.54-7H23c2.242 0 4-2.195 4-5 0-2.803-1.758-5-4-5h-2.54c.79-4.127 2.522-7 4.54-7 2.762 0 5 5.373 5 12s-2.238 12-5 12z' />
    </svg>
  );
};

MegaphoneIcon.propTypes = {
  width: PropTypes.string,
  height: PropTypes.string,
  fillColor: PropTypes.string,
};

export const LeftArrowIcon = (props) => {
  const width = props.width || '27';
  const height = props.height || '19';
  const fillColor = props.fillColor || '#1e201d';

  return (
    <svg
      xmlns='http://www.w3.org/2000/svg'
      width={width} height={height}
      fill={fillColor}
      viewBox={'0 0 31.5 31.5'}
    >
      <path d="M10.273 5.01c.444-.445 1.143-.445 1.587 0 .43.428.43 1.142 0 1.57l-8.047 8.047h26.554c.62 0 1.127.492 1.127 1.11 0 .62-.508 1.128-1.127 1.128H3.813l8.047 8.032c.43.444.43 1.16 0 1.587-.444.444-1.143.444-1.587 0L.32 16.532c-.428-.43-.428-1.143 0-1.57l9.953-9.953z" />
    </svg>
  );
};

LeftArrowIcon.propTypes = {
  width: PropTypes.string,
  height: PropTypes.string,
  fillColor: PropTypes.string,
};
