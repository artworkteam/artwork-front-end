import React, { Component } from 'react';
import PropTypes from 'prop-types';
import uploadcare from 'uploadcare-widget';
import { maxSize, minDimensions } from '../../utils/uploadcareValidators';
import { FILE_MAXIMUM_SIZE, FILE_MINIMUM_WIDTH, FILE_MINIMUM_HEIGHT, SETTINGS } from '../../constants/Uploadcare';
import './uploadcareWidget.css';


export default class UploadcareWidget extends Component {
  static propTypes = {
    imageChange: PropTypes.func.isRequired,
    widgetPlaceholderRef: PropTypes.shape({}),
    imageId: PropTypes.string,
  }

  static defaultProps = {
    imageId: null,
  }

  componentDidMount() {
    const { imageChange } = this.props;
    this.panel = uploadcare.openPanel(this.panelPlaceholder, null, null, SETTINGS);

    this.panel.fileColl.onAnyDone((onDone) => {
      onDone.done((fileInfo) => {
        this.uploadedFromPanel = true;
        imageChange(fileInfo.uuid);
      });
    });
    this.panel.fileColl.onAnyProgress((onProgress) => {
      onProgress.progress((uploadInfo) => {
        if (uploadInfo.progress === 0) imageChange(null);
      });
    });
  }

  componentWillReceiveProps(nextProps) {
    const { widgetPlaceholderRef, imageId, imageChange } = this.props;
    const { widgetPlaceholderRef: nextWidgetPlaceholderRef, imageId: nextImageId } = nextProps;

    if (widgetPlaceholderRef !== nextWidgetPlaceholderRef) {
      this.widget = uploadcare.Widget(nextWidgetPlaceholderRef);

      this.widget.validators.push(minDimensions(FILE_MINIMUM_WIDTH, FILE_MINIMUM_HEIGHT));
      this.widget.validators.push(maxSize(FILE_MAXIMUM_SIZE));

      this.widget.onUploadComplete((fileInfo) => {
        this.uploadedFromPanel = false;
        imageChange(fileInfo.uuid);
      });
    }

    if (imageId !== nextImageId && nextImageId !== null) {
      if (this.uploadedFromPanel) {
        this.widget.value(nextImageId);
      } else {
        const file = uploadcare.fileFrom('uploaded', nextImageId);
        this.panel.addFiles([file]);
      }
    }
  }

  render() {
    return (
      <div ref={(ref) => { this.panelPlaceholder = ref; }} />
    );
  }

}
