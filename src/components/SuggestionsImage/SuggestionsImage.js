import React, { Component } from 'react';
import PropTypes from 'prop-types';
import find from 'lodash/find';
import { showPreview } from '../../actions/SuggestionsActions';
import { push } from '../../actions/RouteActions';
import { SuggestionMarker, SuggestionsMarkerPopover, WorkImage, LoaderSpinner } from '../../components';
import styles from './suggestionsImage.css';

export default class SuggestionsImage extends Component {
  static propTypes = {
    dispatch: PropTypes.func.isRequired,
    me: PropTypes.shape({}).isRequired,
    workId: PropTypes.string.isRequired,
    work: PropTypes.shape({}).isRequired,
    selectedSuggestion: PropTypes.string,
    isAddSuggestionMode: PropTypes.bool.isRequired,
    previewSuggestion: PropTypes.shape({}),
    selectedAuthor: PropTypes.shape({}),
    imageContainerWidth: PropTypes.number.isRequired,
    imageContainerHeight: PropTypes.number.isRequired,
  };

  state = {
    imageLoaded: false,
  }

  imageLoaded = () => {
    this.setState({
      imageLoaded: true,
    });
  }

  gridClick = (e) => {
    if (!this.props.me) {
      return;
    }

    const { dispatch, workId, isAddSuggestionMode } = this.props;

    const { clientHeight, clientWidth } = e.target;
    const { offsetX, offsetY } = e.nativeEvent;

    //getting coordinates in %
    const x = (offsetX / clientWidth) * 100;
    const y = (offsetY / clientHeight) * 100;

    if (!isAddSuggestionMode) {
      dispatch(push(`/works/${workId}/suggestions/add`));
    }
    dispatch(showPreview(x, y));
  }

  renderPreview() {
    const { previewSuggestion, dispatch, selectedAuthor, workId,
      imageContainerWidth, imageContainerHeight,
    } = this.props;
    if (previewSuggestion) {
      return (
      [
        <SuggestionMarker
          {...previewSuggestion}
          dispatch={dispatch}
          selectedAuthor={selectedAuthor}
          workId={workId}
          key={'preview marker'}
        />,
        <SuggestionsMarkerPopover
          {...previewSuggestion}
          workId={workId}
          dispatch={dispatch}
          imageContainerWidth={imageContainerWidth}
          imageContainerHeight={imageContainerHeight}
          key={'preview popup'}
        />,
      ]
      );
    }

    return null;
  }

  renderMarkers() {
    const { selectedSuggestion,
      selectedAuthor, dispatch, workId, work } = this.props;
    const markers = [];

    if (selectedAuthor) {
      markers.push(selectedAuthor.suggestions.map((suggestion) => {
        const coords = { x: suggestion.pointX, y: suggestion.pointY };
        return (
        [
          <SuggestionMarker
            {...coords}
            suggestionId={suggestion.suggestionId}
            key={suggestion.suggestionId}
            selectedSuggestion={selectedSuggestion}
            dispatch={dispatch}
            suggestion={suggestion}
            selectedAuthor={selectedAuthor}
            workId={workId}
            work={work}
          />,
        ]
        );
      }));
    }

    return markers;
  }

  renderPopover() {
    const { selectedAuthor, workId, dispatch, selectedSuggestion,
      imageContainerWidth, imageContainerHeight,
    } = this.props;
    const suggestion =
      selectedAuthor &&
      selectedSuggestion &&
      find(selectedAuthor.suggestions, o => o.suggestionId === selectedSuggestion);

    if (suggestion) {
      const coords = { x: suggestion.pointX, y: suggestion.pointY };
      return (
        <SuggestionsMarkerPopover
          {...coords}
          workId={workId}
          suggestion={suggestion}
          dispatch={dispatch}
          imageContainerWidth={imageContainerWidth}
          imageContainerHeight={imageContainerHeight}
        />
      );
    }

    return null;
  }

  render() {
    const { work } = this.props;
    const { imageLoaded } = this.state;

    return (
      <div className={styles.wrapper}>
        {!imageLoaded &&
          <div className={styles.spinnerContainer}>
            <LoaderSpinner />
          </div>
        }
        <div className={imageLoaded ? styles.suggestionsImage : styles.suggestionsImageLoading}>
          <div className={styles.markerksGrid} onClick={this.gridClick} />
          { this.renderPreview() }
          { this.renderMarkers() }
          { this.renderPopover() }
          <WorkImage
            imageId={work.imageId}
            type='fullSize'
            className={styles.image}
            onLoad={this.imageLoaded}
            onError={this.imageLoaded}
          />
        </div>
      </div>
    );
  }
}
