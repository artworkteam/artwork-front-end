import React from 'react';
import PropTypes from 'prop-types';
import styles from './modalCloseButton.css';

const ModalCloseButton = ({ closeModal }) => (
  <button type="button" className={styles.modalCloseButton} onClick={closeModal}>×</button>
);

ModalCloseButton.propTypes = {
  closeModal: PropTypes.func.isRequired,
};

export default ModalCloseButton;
