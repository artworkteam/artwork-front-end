import AppContainer from './containers/AppContainer';
import WorksContainer from './containers/WorksContainer';
import WorkContainer from './containers/WorkContainer';
import SuggestionsContainer from './containers/SuggestionsContainer';
import ServerNotificationsContainer from './containers/ServerNotificationContainer';
import { About, NotFound } from './components/';

export default {
  path: '/(works)',
  component: AppContainer,
  indexRoute: { component: WorksContainer },
  childRoutes: [
    {
      path: '/works/:workId',
      component: WorkContainer,
    },
    {
      path: '/works/:workId/suggestions(/:add)',
      component: SuggestionsContainer,
    },
    {
      path: '/notifications',
      component: ServerNotificationsContainer,
    },
    {
      path: '/about',
      component: About,
    },
    {
      path: '/404',
      component: NotFound,
    },
    {
      path: '*',
      onEnter: ({ params }, replace) => replace('/404'),
    },
  ],
};
