import { PROVIDERS } from '../constants/Config';

const settings = 'scrollbars=no,toolbar=no,location=no,titlebar=no,directories=no,status=no,menubar=no';

function getPopupOffset({ width, height }) {
  const wLeft = window.screenLeft ? window.screenLeft : window.screenX;
  const wTop = window.screenTop ? window.screenTop : window.screenY;

  const left = wLeft + ((window.innerWidth / 2) - (width / 2));
  const top = wTop + ((window.innerHeight / 2) - (height / 2));

  return { top, left };
}

function getPopupDimensions({ width, height }) {
  const { top, left } = getPopupOffset({ width, height });

  return `width=${width},height=${height},top=${top},left=${left}`;
}

export default function openPopup(provider) {
  const { name, url, popupSize } = PROVIDERS[provider];

  return window.open(url, name, `${settings},${getPopupDimensions(popupSize)}`);
}
