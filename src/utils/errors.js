export const isNetworkError = error => error.type === 'network_error';
const networkError = () => ({
  type: 'network_error',
});

export const isServerError = error => error.type === 'server_error';
const serverError = status => ({
  type: 'server_error',
  status,
});

export const isAuthError = error => error.type === 'auth_error';
const authError = (message, path) => ({
  type: 'auth_error',
  message,
  path,
});

export const isValidationError = error => error.type === 'validation_error';
const validationError = fields => ({
  type: 'validation_error',
  fields,
});

export const isGeneralError = error => error.type === 'error';
const generalError = error => ({
  type: 'error',
  ...error,
});

export const isUnknownError = error => error.type === 'unknown_error';
const unknownError = () => ({
  type: 'unknown_error',
});

export const parseError = (rawError) => {
  console.error(rawError);
  const { response } = rawError;
  if (!response) return networkError();

  const { status, data = {} } = response;
  if (status >= 500 && status <= 599) return serverError(status);
  if (data.error && data.path && data.status === 401) return authError(data.message, data.path);
  if (data.errors) {
    const validationErrors = [];
    data.errors.forEach((e) => {
      if (e.property) validationErrors.push(e);
    });

    if (validationErrors) {
      const fields = {};
      validationErrors.forEach(({ property, ...e }) => {
        const list = fields[property] || [];
        list.push(e);
        fields[property] = list;
      });
      return validationError(fields);
    }
    return generalError(data.errors[0]);
  }
  return unknownError();
};
