import 'whatwg-fetch';
import { API_URL } from '../constants/Config';

export default function (url, options = {}) {
  return fetch(API_URL + url, { ...options, credentials: 'include' })
    .then(data => Promise.resolve(data))
    .catch(error => Promise.reject(error));
}
