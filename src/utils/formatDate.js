import pluralize from 'pluralize-ru';

const MS_PER_MINUTE = 1000 * 60;
const MS_PER_HOUR = 1000 * 60 * 60;
const MS_PER_DAY = 1000 * 60 * 60 * 24;

const dateDiffInDays = (a, b) => {
  const utc1 = Date.UTC(a.getFullYear(), a.getMonth(), a.getDate());
  const utc2 = Date.UTC(b.getFullYear(), b.getMonth(), b.getDate());

  return Math.floor((utc2 - utc1) / MS_PER_DAY);
};
const dateDiffInMinutes = (a, b) => Math.floor(((b.getTime() - a.getTime()) / MS_PER_MINUTE));
const dateDiffInHours = (a, b) => Math.floor(((b.getTime() - a.getTime()) / MS_PER_HOUR));

const formatMinutes = num => pluralize(num, '%d минут назад', '%d минуту назад', '%d минуты назад', '%d минут назад');
const formatHours = num => pluralize(num, '%d часов назад', '%d час назад', '%d часа назад', '%d часов назад');

const getHoursAndMinutes = (date) => {
  const hours = date.getHours();
  const minutes = date.getMinutes();
  const formatedMinutes = minutes < 10 ? `0${minutes}` : `${minutes}`;

  return `${hours}:${formatedMinutes}`;
};

export default (timestamp) => {
  const date = new Date(timestamp);
  const currentlyDate = new Date();
  const minutesDiff = dateDiffInMinutes(date, currentlyDate);
  const hoursDiff = dateDiffInHours(date, currentlyDate);
  const daysDiff = dateDiffInDays(date, currentlyDate);

  if (minutesDiff < 1) {
    return 'прямо сейчас';
  }
  if (hoursDiff < 1) {
    return formatMinutes(minutesDiff);
  }
  if (daysDiff < 1) {
    return formatHours(hoursDiff);
  }
  if (daysDiff === 1) {
    return `вчера в ${getHoursAndMinutes(date)}`;
  }

  return `${date.toLocaleDateString()} в ${getHoursAndMinutes(date)}`;
};

export const formatTime = timestamp => getHoursAndMinutes(new Date(timestamp));
export const formatDate = (timestamp) => {
  const date = new Date(timestamp);
  const currentlyDate = new Date();
  const daysDiff = dateDiffInDays(date, currentlyDate);

  switch (daysDiff) {
    case 0:
      return 'Сегодня';
    case 1:
      return 'Вчера';
    default:
      return date.toLocaleDateString();
  }
};
