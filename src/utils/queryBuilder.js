export default function (queryObj) {
  const result = [];

  Object.keys(queryObj).forEach((key) => {
    const value = queryObj[key];
    if (value != null) {
      const encoded = encodeURIComponent(value);
      result.push(`${key}=${encoded}`);
    }
  });

  return result.join('&');
}
