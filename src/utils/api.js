import axios from 'axios';
import { API_URL } from '../constants/Config';

const formatUrl = url => API_URL + url;

export default {
  get: (url, params) => config => axios.get(formatUrl(url), {
    ...config,
    params,
    withCredentials: true,
  }),
  post: (url, data, params) => config => axios.post(formatUrl(url), data, {
    ...config,
    params,
    withCredentials: true,
  }),
  patch: (url, data, params) => config => axios.patch(formatUrl(url), data, {
    ...config,
    params,
    withCredentials: true,
  }),
  put: (url, data, params) => config => axios.put(formatUrl(url), data, {
    ...config,
    params,
    withCredentials: true,
  }),
  delete: (url, params) => config => axios.delete(formatUrl(url), {
    ...config,
    params,
    withCredentials: true,
  }),
};
