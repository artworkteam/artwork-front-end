import { mapValues } from 'lodash';
import { SubmissionError } from 'redux-form';
import { isValidationError } from './errors';

export const handleSubmitError = promise => promise.catch((error = {}) => {
  console.log(error);
  if (isValidationError(error)) {
    throw new SubmissionError(mapValues(
      error.fields,
      f => f.map(e => ({ code: e.code, args: e.arguments })),
    ));
  }
  throw new SubmissionError(error.code || error.type);
});
