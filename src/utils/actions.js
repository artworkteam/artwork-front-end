import { CancelToken, isCancel } from 'axios';
import { parseError } from './errors';

export const isRequestAction = action => action.api && action.api === 'request';
export const isResultAction = action => action.api && action.api === 'result';
export const isErrorAction = action => isResultAction(action) && action.error;
export const isSuccessAction = action => isResultAction(action) && !action.error;

const apiActionRequest = type => ({
  type,
  api: 'request',
});

const apiResultAction = (type, data) => ({
  type,
  payload: data,
  api: 'result',
});

const apiErrorAction = (type, error) => ({
  type,
  error: true,
  payload: error,
  api: 'result',
});

export const createApiAction = (type, req, reader, { onlyLast = false } = {}) => {
  let cancelLast = false;

  return (...args) => (dispatch) => {
    const config = {};
    if (onlyLast) {
      if (cancelLast) cancelLast();
      const { token, cancel } = CancelToken.source();
      config.cancelToken = token;
      cancelLast = cancel;
    }

    dispatch(apiActionRequest(type));
    return req(...args)(config)
      .then(reply => dispatch(apiResultAction(type, reader ? reader(reply.data) : reply.data)))
      .catch((rawError) => {
        if (isCancel(rawError)) console.log(`${type} canceled`);
        else {
          const error = parseError(rawError);
          dispatch(apiErrorAction(type, error));
          throw error;
        }
      });
  };
};
