export const maxSize = size => (fileInfo) => {
  if (fileInfo.size !== null && fileInfo.size > size) {
    throw new Error('size');
  }
};

export const minDimensions = (width, height) => (fileInfo) => {
  const imageInfo = fileInfo.originalImageInfo;
  if (imageInfo === null) {
    return;
  }
  const heightLose = height && imageInfo.height < height;
  if (width && imageInfo.width < width) {
    if (heightLose) {
      throw new Error('minDimensions');
    } else {
      throw new Error('minWidth');
    }
  }
  if (heightLose) {
    throw new Error('minHeight');
  }
};
