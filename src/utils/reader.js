import { normalize } from 'normalizr';

export const createEntityReader = schema => data => normalize(data, schema);
export const createPageReader = (schema, name) => (data) => {
  // eslint-disable-next-line no-underscore-dangle
  const list = (data._embedded && data._embedded[name]) || [];
  const normalized = normalize(list, schema);
  return { ...normalized, page: data.page };
};
