const validate = (obj, values) => {
  const errors = {};
  for (const k in obj) {
    const property = obj[k];
    const value = values[k];
    let error = null;
    if (typeof property === 'object') {
      error = validate(property, value || {});
    } else if (typeof property === 'function') {
      error = property(value);
    }
    if (error) errors[k] = error;
  }
  return errors;
};

export default obj => values => validate(obj, values);

const error = (code, ...args) => ({
  code,
  args,
});

export const valid = (...args) => (value) => {
  const errors = [];
  args.forEach((v) => {
    const e = v(value);
    if (e) errors.push(e);
  });
  return errors.length > 0 ? errors : null;
};

export const notEmpty = () => (value) => {
  if (!value) return error('NotEmpty');
  return null;
};

export const size = (min, max) => (value) => {
  if (value && (value.length > max || value.length < min)) return error('Size', max, min);
  return null;
};
