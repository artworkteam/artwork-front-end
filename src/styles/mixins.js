module.exports = {
  container(mixin, direction = 'row') {
    return {
      'lost-center': '1170px 15px flex',
      'lost-flex-container': direction,
      'flex-grow': '1',
      '@media (--large)': {
        'lost-center': '970px 15px flex',
      },
      '@media (--medium)': {
        'lost-center': '750px 15px flex',
      },
    };
  },
};
