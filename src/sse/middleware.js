import { API_URL } from '../constants/Config';

export const isSSESupported = (typeof EventSource !== 'undefined');

const SSE_CONNECT = 'SSE_CONNECT';
export const sseConnect = (name, path, onOpen, onMessage, onError) => ({
  type: SSE_CONNECT,
  name,
  path,
  onOpen,
  onMessage,
  onError,
});

const SSE_DISCONNECT = 'SSE_DISCONNECT';
export const sseDisconnect = name => ({
  type: SSE_DISCONNECT,
  name,
});

export default ({ dispatch }) => {
  const connections = {};
  const closeConnection = (name) => {
    if (connections[name]) {
      connections[name].close();
      connections[name] = null;
      return true;
    }
    return false;
  };
  const onConnect = ({ name, path, onOpen, onMessage, onError }) => {
    if (connections[name]) return false;
    const src = new EventSource(API_URL + path, { withCredentials: true });
    src.onmessage = (msg) => {
      console.log(`Recived message for ${name} from ${path}`, msg);
      onMessage(dispatch, msg.data);
    };
    connections[name] = src;
    console.log(`Openning ${name} connection to ${path}`, connections);
    return true;
  };
  const onDisconnect = ({ name }) => closeConnection(name);

  return next => (action) => {
    if (!isSSESupported) return next(action);
    switch (action.type) {
      case SSE_CONNECT:
        next(action);
        return onConnect(action);
      case SSE_DISCONNECT:
        next(action);
        return onDisconnect(action);
      default:
        return next(action);
    }
  };
};
