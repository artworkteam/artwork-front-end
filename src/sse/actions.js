import { sseConnect } from './middleware';

export const isConnectAction = action => action.sse && action.sse === 'connected';
export const isErrorAction = action => action.sse && action.sse === 'error';
export const isMessageAction = action => action.sse && action.sse === 'message';

export const createSseAction = (name, type, url, reader) => {
  const onOpen = dispatch => dispatch({
    type,
    sse: 'connected',
  });
  const onError = dispatch => dispatch({
    type,
    sse: 'error',
  });
  const onMessage = (dispatch, data) => dispatch({
    type,
    sse: 'message',
    payload: reader ? reader(JSON.parse(data)) : data,
  });
  return () => sseConnect(name, url, onOpen, onMessage, onError);
};
