import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';
import auth from '../reducers/auth';
import entities from '../reducers/entities';
import modal from '../reducers/modal';
import suggestions from '../reducers/suggestions';
import work from '../reducers/work';
import works from '../reducers/works';
import alerts from '../reducers/alerts';
import comments from '../reducers/comments';
import serverNotifications from '../reducers/server-notifications';

export default combineReducers({
  auth,
  entities,
  modal,
  suggestions,
  work,
  works,
  alerts,
  comments,
  serverNotifications,
  form: formReducer,
});
