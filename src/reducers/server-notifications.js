import _ from 'lodash';
import { isRequestAction, isSuccessAction, isErrorAction } from '../utils/actions';
import { isMessageAction } from '../sse/actions';
import {
  CLEAR_NOTIFICATIONS,
  FETCH_NOTIFICATION,
  FETCH_UNREAD_NOTIFICATION,
  NOTIFICATIONS_SSE,
  READ_NOTIFICATION,
  MASS_READ_NOTIFICATIONS,
} from '../constants/ServerNotifications';

const initialState = {
  loading: false,
  items: [],
  nextPage: 0,
  hasMore: true,
  unread: [],
  error: null,
};

// Getters
export const getNotifications = ({ items }) => items || [];

export const getUnread = ({ unread }) => unread;
export const hasNew = ({ unread }) => unread.length > 0;

const onClear = state => ({
  ...state,
  loading: false,
  items: [],
  nextPage: 0,
  hasMore: true,
  error: null,
});

const onNotificationsFetched = (state, action) => {
  if (isRequestAction(action)) return { ...state, loading: true };
  if (isSuccessAction(action)) {
    const { result, page } = action.payload;
    return {
      ...state,
      loading: false,
      items: state.items.concat(result),
      nextPage: page.number + 1,
      hasMore: page.number + 1 < page.totalPages,
    };
  }
  if (isErrorAction(action)) return { ...state, loading: false, error: action.error };
  return state;
};

const onUnreadFetched = (state, action) => {
  if (isRequestAction(action)) return { ...state };
  if (isSuccessAction(action)) {
    const { result } = action.payload;
    return {
      ...state,
      unread: result,
    };
  }
  if (isErrorAction(action)) return { ...state, error: action.error };
  return state;
};

const onSseAction = (state, action) => {
  if (isMessageAction(action)) {
    const { result } = action.payload;
    return {
      ...state,
      unread: [...state.unread, result],
    };
  }
  return state;
};

const onReadNotification = (state, action) => {
  if (isSuccessAction(action)) {
    const { result } = action.payload;
    return {
      ...state,
      unread: _.without(state.unread, result),
    };
  }
  return state;
};

const onMassReadNotifications = (state, action) => {
  if (isSuccessAction(action)) {
    const { result } = action.payload;
    return {
      ...state,
      unread: _.without(state.unread, ...result),
    };
  }
  return state;
};

export default (state = initialState, action) => {
  switch (action.type) {
    case CLEAR_NOTIFICATIONS:
      return onClear(state, action);
    case FETCH_NOTIFICATION:
      return onNotificationsFetched(state, action);
    case FETCH_UNREAD_NOTIFICATION:
      return onUnreadFetched(state, action);
    case NOTIFICATIONS_SSE:
      return onSseAction(state, action);
    case READ_NOTIFICATION:
      return onReadNotification(state, action);
    case MASS_READ_NOTIFICATIONS:
      return onMassReadNotifications(state, action);
    default:
      return state;
  }
};
