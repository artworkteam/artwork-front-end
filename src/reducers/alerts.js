import {
  SHOW_ERROR_ALERT,
  SHOW_SUCCESS_ALERT,
  CLEAR_ALERT,
  NETWORK_OR_SERVER_ERROR,
  ERROR_ALERT,
  SUCCESS_ALERT,
} from '../constants/Alerts';
import { isErrorAction } from '../utils/actions';
import { isValidationError, isAuthError } from '../utils/errors';

const initialState = {
  message: null,
  type: null,
};


const setErrorAlert = (state, message) =>
  ({ ...state, message, type: ERROR_ALERT });
const setSuccessAlert = (state, message) =>
  ({ ...state, message, type: SUCCESS_ALERT });

const onErrorAction = (state, payload) => {
  if (!isValidationError(payload) || (isAuthError(payload) && payload.path !== '/login')) {
    return setErrorAlert(state, NETWORK_OR_SERVER_ERROR);
  }
  return state;
};

export default function alerts(state = initialState, action) {
  if (isErrorAction(action)) return onErrorAction(state, action.payload);
  switch (action.type) {
    case SHOW_ERROR_ALERT:
      return setErrorAlert(state, action.payload);
    case SHOW_SUCCESS_ALERT:
      return setSuccessAlert(state, action.payload);
    case CLEAR_ALERT:
      return { ...state, message: null, type: null };
    default:
      return state;
  }
}
