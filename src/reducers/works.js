import { isRequestAction, isSuccessAction, isErrorAction } from '../utils/actions';
import {
  CLEAR_WORKS,
  FETCH_WORKS,
  PIN_WORK_BY_ID,
} from '../constants/Works';

const initialState = {
  loading: false,
  items: [],
  nextPage: 0,
  hasMore: true,
  pinnedItem: null,
};

const onWorksFetched = (state, action) => {
  if (isRequestAction(action)) return { ...state, loading: true };
  if (isSuccessAction(action)) {
    const { result, page } = action.payload;
    return {
      ...state,
      loading: false,
      items: state.items.concat(result),
      nextPage: page.number + 1,
      hasMore: page.number + 1 < page.totalPages,
    };
  }
  if (isErrorAction(action)) return { ...state, loading: false };
  return state;
};

const onWorkPinned = (state, action) => {
  if (isRequestAction(action)) return { ...state, loading: true };
  if (isSuccessAction(action)) {
    const { result } = action.payload;
    return {
      ...state,
      loading: false,
      pinnedItem: result[0],
    };
  }
  if (isErrorAction(action)) return { ...state, loading: false };
  return state;
};

export default function works(state = initialState, action) {
  switch (action.type) {
    case CLEAR_WORKS:
      return {
        ...state,
        loading: false,
        items: [],
        nextPage: 0,
        hasMore: true,
      };
    case FETCH_WORKS:
      return onWorksFetched(state, action);
    case PIN_WORK_BY_ID:
      return onWorkPinned(state, action);
    default:
      return state;
  }
}
