import { SHOW_MODAL, HIDE_MODAL } from '../constants/Modal';

const initialState = {
  modalType: null,
};

export default function works(state = initialState, action) {
  switch (action.type) {
    case SHOW_MODAL:
      return { ...state, modalType: action.modalType };

    case HIDE_MODAL:
      return initialState;

    default:
      return state;
  }
}
