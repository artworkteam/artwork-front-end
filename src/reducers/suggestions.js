import {
  GET_SUGGESTIONS_REQUEST,
  GET_SUGGESTIONS_SUCCES,
  GET_SUGGESTIONS_ERROR,
  MARK_SUGGESTION_READED_REQUEST,
  SELECT_SUGGESTION,
  SHOW_PREVIEW,
  HIDE_PREVIEW,
  RESET_SELECTED,
  INITIAL_SUGGESTIONS_START,
  INITIAL_SUGGESTIONS_END,
} from '../constants/Suggestions';

const initialState = {
  loading: false,
  initial: true,
  selectedSuggestion: null,
  previewSuggestion: null,
  suggestionsByAuthor: [],
  error: null,
};

export default function suggestions(state = initialState, action) {
  switch (action.type) {
    case GET_SUGGESTIONS_REQUEST:
      return { ...state, loading: true, error: null };

    case GET_SUGGESTIONS_SUCCES:
      return {
        ...state,
        loading: false,
        suggestionsByAuthor: action.suggestions,
        error: null,
      };

    case GET_SUGGESTIONS_ERROR:
      return { ...state, suggestionsByAuthor: [], loading: false, error: action.error.message };

    case SELECT_SUGGESTION:
      return { ...state, selectedSuggestion: action.suggestionId, previewSuggestion: null };

    case SHOW_PREVIEW:
      return { ...state, selectedSuggestion: null, previewSuggestion: action.coords };

    case HIDE_PREVIEW:
      return { ...state, previewSuggestion: null };

    case RESET_SELECTED:
      return { ...state, selectedSuggestion: null, previewSuggestion: null };

    case INITIAL_SUGGESTIONS_START:
      return { ...state, initial: true };

    case INITIAL_SUGGESTIONS_END:
      return { ...state, initial: false };

    default:
      return state;
  }
}
