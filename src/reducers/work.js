import {
  GET_WORK_REQUEST,
  GET_WORK_SUCCES,
  GET_WORK_ERROR,
  GET_WORK_COMMENTS_REQUEST,
  GET_WORK_COMMENTS_SUCCES,
  GET_WORK_COMMENTS_ERROR,
} from '../constants/Work';

const initialState = {
  loading: false,
  comments: [],
  error: null,
};

export default function work(state = initialState, action) {
  switch (action.type) {
    case GET_WORK_REQUEST:
      return { ...state, loading: true, error: null };

    case GET_WORK_SUCCES:
      return {
        ...state,
        loading: false,
        error: null,
      };

    case GET_WORK_ERROR:
      return { ...state, loading: false, error: action.error.message };

    case GET_WORK_COMMENTS_REQUEST:
      return { ...state, loading: true, error: null };

    case GET_WORK_COMMENTS_SUCCES:
      return {
        ...state,
        loading: false,
        comments: action.comments,
        error: null,
      };

    case GET_WORK_COMMENTS_ERROR:
      return { ...state, loading: false, error: action.error.message };

    default:
      return state;
  }
}
