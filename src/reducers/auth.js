import {
  GET_AUTHED_USER_REQUEST,
  GET_AUTHED_USER_SUCCES,
  GET_AUTHED_USER_ERROR,
  LOGOUT_AUTHED_USER_SUCCES,
  OAUTH_SIGN_IN_START,
  OAUTH_SIGN_IN_SUCCES,
  OAUTH_SIGN_IN_ERROR,
} from '../constants/Auth';

const initialState = {
  loading: false,
  error: null,
  provider: '',
  user: null,
};

export default function auth(state = initialState, action) {
  switch (action.type) {
    case GET_AUTHED_USER_REQUEST:
      return { ...state, loading: true, error: null };

    case GET_AUTHED_USER_SUCCES:
      return { ...state, loading: false, error: null, user: action.payload };

    case GET_AUTHED_USER_ERROR:
      return { ...state, loading: false, error: action.error.message };

    case LOGOUT_AUTHED_USER_SUCCES:
      return initialState;

    case OAUTH_SIGN_IN_START:
      return { ...state, loading: true, error: null, provider: action.provider };

    case OAUTH_SIGN_IN_SUCCES:
      return { ...state, loading: false, error: null };

    case OAUTH_SIGN_IN_ERROR:
      return { ...state, loading: false, error: action.error.message };

    default:
      return state;
  }
}
