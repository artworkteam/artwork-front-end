import merge from 'lodash/merge';
import { isSuccessAction } from '../utils/actions';
import { isMessageAction } from '../sse/actions';

const initialState = {
  users: {},
  works: {},
  comments: {},
};

const onSuccessAction = (state, { payload }) =>
  (payload.entities ? merge({}, state, payload.entities) : state);

export default function entities(state = initialState, action) {
  if (isSuccessAction(action) || isMessageAction(action)) return onSuccessAction(state, action);
  if (action.entities) {
    return merge({}, state, action.entities);
  }
  return state;
}
