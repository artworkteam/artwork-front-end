import { CLEAR_COMMENTS, FETCH_COMMENTS, POST_COMMENT } from '../constants/Comments';
import { isRequestAction, isSuccessAction, isErrorAction } from '../utils/actions';

const initialState = {
  loading: false,
  items: [],
};

const onCommentsFetched = (state, action) => {
  if (isRequestAction(action)) return { ...state, loading: true };
  if (isSuccessAction(action)) {
    const { result } = action.payload;
    return {
      ...state,
      loading: false,
      items: result,
    };
  }
  if (isErrorAction(action)) return { ...state, loading: false };
  return state;
};

const onCommentPosted = (state, action) => {
  if (isSuccessAction(action)) {
    const { result } = action.payload;
    return {
      ...state,
      items: [result].concat(state.items),
    };
  }
  return state;
};

export default function comments(state = initialState, action) {
  switch (action.type) {
    case CLEAR_COMMENTS:
      return initialState;
    case FETCH_COMMENTS:
      return onCommentsFetched(state, action);
    case POST_COMMENT:
      return onCommentPosted(state, action);
    default:
      return state;
  }
}
