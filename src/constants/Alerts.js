export const NETWORK_OR_SERVER_ERROR = 'Упс! Что-то пошло не так. Приносим свои изинения.';

export const SHOW_ERROR_ALERT = 'SHOW_ERROR_ALERT';
export const SHOW_SUCCESS_ALERT = 'SHOW_SUCCESS_ALERT';
export const CLEAR_ALERT = 'CLEAR_ALERT';

export const ERROR_ALERT = 'ERROR_ALERT';
export const SUCCESS_ALERT = 'SUCCESS_ALERT';
