export const DEV_API_HOSTNAME = 'dev-api.artwork.pro';
export const API_HOSTNAME = process.env.NODE_ENV === 'production'
? 'api.artwork.pro'
: DEV_API_HOSTNAME;

export const API_URL = DEV_SERVER ? '' : `https://${API_HOSTNAME}`;

export const APPLICATION_HOSTNAME = process.env.NODE_ENV === 'production'
? 'www.artwork.pro'
: 'dev.artwork.pro';

export const PROVIDERS = {
  vkontakte: {
    displayName: 'Вконтакте',
    url: `https://${API_HOSTNAME}/api/login/vkontakte`,
    popupSize: { width: 720, height: 390 },
  },
  facebook: {
    displayName: 'Facebook',
    url: `https://${API_HOSTNAME}/api/login/facebook`,
    popupSize: { width: 720, height: 551 },
  },
};
