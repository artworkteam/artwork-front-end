import { schema } from 'normalizr';

export const userSchema = new schema.Entity('users', {}, { idAttribute: 'uuid' });
export const statisticsSchema = new schema.Entity('users', { }, {
  idAttribute: 'statisticsId',
  processStrategy: entity => new Object({
    ...entity,
    ...entity.owner,
    uuid: entity.statisticsId,
  }),
});
export const workSchema = new schema.Entity('works', { owner: userSchema }, { idAttribute: 'workId' });
export const workListSchema = [workSchema];
export const commentSchema = new schema.Entity('comments', { author: userSchema }, { idAttribute: 'commentId' });
export const commentListSchema = [commentSchema];
export const notificationSchema = new schema.Entity('notifications', {}, { idAttribute: 'notificationId' });
export const notificationListSchema = [notificationSchema];
