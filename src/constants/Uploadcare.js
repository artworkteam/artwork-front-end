import { maxSize, minDimensions } from '../utils/uploadcareValidators';

export const CDN_HOSTNAME = 'ucarecdn.com';

export const FILE_MAXIMUM_SIZE = 5 * 1024 * 1024; //the first num is mb count.

export const FILE_MINIMUM_WIDTH = 800; //in px
export const FILE_MINIMUM_HEIGHT = 600;

export const SETTINGS = {
  publicKey: 'b8b38bfa6226e52ff22a',
  tabs: [
    'file',
  ].join(' '),
  imagesOnly: true,
  previewStep: true,
  validators: [
    minDimensions(FILE_MINIMUM_WIDTH, FILE_MINIMUM_HEIGHT),
    maxSize(FILE_MAXIMUM_SIZE)
  ],
};
