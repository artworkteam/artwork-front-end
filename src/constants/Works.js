export const CLEAR_WORKS = 'CLEAR_WORKS';
export const FETCH_WORKS = 'FETCH_WORKS';
export const ADD_WORK = 'ADD_WORK';
export const DELETE_WORK = 'DELETE_WORK';
export const LIKE_WORK = 'LIKE_WORK';
export const PIN_WORK_BY_ID = 'PIN_WORK_BY_ID';
export const UPDATE_WORK_DESCRIPTION = 'UPDATE_WORK_DESCRIPTION';

export const SORTS = {
    date: 'date',
    rating: 'rating',
    popularity: 'popularity',
};
