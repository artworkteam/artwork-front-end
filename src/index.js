import React from 'react';
import ReactDOM from 'react-dom';
import LogRocket from 'logrocket';
import { AppContainer as HotContainer } from 'react-hot-loader';
import configureStore from './store/configureStore';
import App from './App';
import './styles/main.css';

if (process.env.NODE_ENV === 'production') {
  require('logrocket-react')(LogRocket);
  LogRocket.init('ijiy8m/artwork');
}


const store = configureStore();

const render = () => {
  ReactDOM.render(
    <HotContainer>
      <App store={store} />
    </HotContainer>,
    document.getElementById('root'),
  );
};

render();

// uncomment for hot-loader enable.
if (module.hot) {
  module.hot.accept('./App', () => {
    render();
  });
}
