import { normalize } from 'normalizr';
import { fetch, checkStatus, parseResponse } from '../utils';
import {
  GET_USER_REQUEST,
  GET_USER_SUCCES,
  GET_USER_ERROR,
} from '../constants/Users';
import { userSchema } from '../constants/Schemas';


export function getUserRequest() {
  return {
    type: GET_USER_REQUEST,
  };
}

export function getUserSucces(entities) {
  return {
    type: GET_USER_SUCCES,
    entities,
  };
}

export function getUserError(error) {
  return {
    type: GET_USER_ERROR,
    error,
  };
}

export function getUser(userId) {
  return (dispatch) => {
    dispatch(getUserRequest());

    return fetch(`/api/users/${userId}/statistics`)
      .then(checkStatus)
      .then(parseResponse)
      .then((data) => {
        const normalized = normalize(data, userSchema);
        const entities = normalized.entities;

        dispatch(getUserSucces(entities));
      })
      .catch(error => dispatch(getUserError(error)));
  };
}
