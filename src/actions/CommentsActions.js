import { createApiAction } from '../utils/actions';
import { createEntityReader, createPageReader } from '../utils/reader';
import { NO_DATA } from '../constants/Api';
import api from '../utils/api';
import { CLEAR_COMMENTS, FETCH_COMMENTS, LIKE_COMMENT, POST_COMMENT } from '../constants/Comments';
import { commentSchema, commentListSchema } from '../constants/Schemas';

const commentReader = createEntityReader(commentSchema);
const commentPageReader = createPageReader(commentListSchema, 'commentResourceList');

export const clearComments = () => ({ type: CLEAR_COMMENTS });

export const getComments = createApiAction(
  FETCH_COMMENTS,
  (id, last = 500) => api.get(
    `/api/commentable/${id}/comments`,
    { page: 0, size: last, sort: 'createdDate,desc' },
  ),
  commentPageReader,
  { onlyLast: true },
);

export const likeComment = createApiAction(
  LIKE_COMMENT,
  (commentableId, commentId, like) => api.put(
    `/api/commentable/${commentableId}/comments/${commentId}`,
    NO_DATA,
    { like },
  ),
  commentReader,
);

export const postComment = createApiAction(
  POST_COMMENT,
  (id, data) => api.post(`/api/commentable/${id}/comments`, data),
  commentReader,
);
