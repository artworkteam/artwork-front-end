import { createApiAction } from '../utils/actions';
import { createEntityReader, createPageReader } from '../utils/reader';
import { NO_DATA } from '../constants/Api';
import api from '../utils/api';
import {
  CLEAR_WORKS,
  FETCH_WORKS,
  ADD_WORK,
  DELETE_WORK,
  LIKE_WORK,
  PIN_WORK_BY_ID,
  UPDATE_WORK_DESCRIPTION,
  SORTS,
} from '../constants/Works';
import { workSchema, workListSchema } from '../constants/Schemas';

const workReader = createEntityReader(workSchema);
const worksPageReader = createPageReader(workListSchema, 'workResourceList');

export const clearWorks = () => ({ type: CLEAR_WORKS });

export const getWorks = createApiAction(
  FETCH_WORKS,
  ({ page = 0, size = 10, filter, byUser: ownerId } = {}) =>
    api.get('/api/works', { page, size, ownerId, sort: SORTS[filter] }),
  worksPageReader,
  { onlyLast: true },
);

export const addWork = createApiAction(
  ADD_WORK,
  data => api.post('/api/works/', data),
);

export const deleteWork = createApiAction(
  DELETE_WORK,
  (workId) => {
    return api.delete(`/api/works/${workId}`);
  }
);

export const likeWork = createApiAction(
  LIKE_WORK,
  (workId, like) => api.put(`/api/works/${workId}`, NO_DATA, { like }),
  workReader,
  { onlyLast: true },
);

export const pinWorkById = createApiAction(
  PIN_WORK_BY_ID,
  id => api.get(`/api/works/${id}`),
  workReader,
  { onlyLast: true },
);

export const updateWorkDescription = createApiAction(
  UPDATE_WORK_DESCRIPTION,
  (id, data) => api.put(`/api/works/${id}`, data),
  workReader,
);
