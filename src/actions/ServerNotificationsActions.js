import { createApiAction } from '../utils/actions';
import { createSseAction } from '../sse/actions';
import { sseDisconnect } from '../sse/middleware';
import { createEntityReader, createPageReader } from '../utils/reader';
import { NO_DATA } from '../constants/Api';
import api from '../utils/api';
import {
  FETCH_NOTIFICATION,
  READ_NOTIFICATION,
  MASS_READ_NOTIFICATIONS,
  CLEAR_NOTIFICATIONS,
  FETCH_UNREAD_NOTIFICATION,
  SSE_CONNECTION_NAME,
  NOTIFICATIONS_SSE,
} from '../constants/ServerNotifications';
import { notificationSchema, notificationListSchema } from '../constants/Schemas';

const notificationReader = createEntityReader(notificationSchema);
const notificationListReader = createEntityReader(notificationListSchema);
const notificationsPageReader = createPageReader(notificationListSchema, 'notificationResourceList');

export const clearNotifications = () => ({
  type: CLEAR_NOTIFICATIONS,
});

export const fetchNotifications = createApiAction(
  FETCH_NOTIFICATION,
  ({ page = 0, size = 10, sort = 'createdDate,desc' } = {}) => api.get('/api/notifications', { page, size, sort }),
  notificationsPageReader,
  { onlyLast: true },
);

export const readNotification = createApiAction(
  READ_NOTIFICATION,
  uid => api.put(`/api/notifications/${uid}`, NO_DATA, { read: true }),
  notificationReader,
);

export const massReadNotifications = createApiAction(
  MASS_READ_NOTIFICATIONS,
  uids => api.post('/api/notifications/read', { ids: uids }),
  notificationListReader,
);

export const fetchUnreadNotifications = createApiAction(
  FETCH_UNREAD_NOTIFICATION,
  ({ page = 0, size = 999 } = {}) => api.get('/api/notifications', { page, size, read: false }),
  notificationsPageReader,
  { onlyLast: true },
);

export const openNotificationsSse = createSseAction(
  SSE_CONNECTION_NAME,
  NOTIFICATIONS_SSE,
  '/api/notifications/stream',
  notificationReader,
);

export const closeNotificationsSse = () => sseDisconnect(SSE_CONNECTION_NAME);
