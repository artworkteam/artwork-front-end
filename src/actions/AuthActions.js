import Cookies from 'react-cookie';
import LogRocket from 'logrocket';
import {
  GET_AUTHED_USER_REQUEST,
  GET_AUTHED_USER_SUCCES,
  GET_AUTHED_USER_ERROR,
  LOGOUT_AUTHED_USER_SUCCES,
  OAUTH_SIGN_IN_START,
  OAUTH_SIGN_IN_SUCCES,
  OAUTH_SIGN_IN_ERROR,
} from '../constants/Auth';
import { APPLICATION_HOSTNAME } from '../constants/Config';
import { fetch, checkStatus, parseResponse, openPopup } from '../utils';

export function getAuthedUserRequest() {
  return {
    type: GET_AUTHED_USER_REQUEST,
  };
}

export function getAuthedUserSucces(payload) {
  return {
    type: GET_AUTHED_USER_SUCCES,
    payload,
  };
}

export function getAuthedUserError(error) {
  return {
    type: GET_AUTHED_USER_ERROR,
    error,
  };
}

export function logoutAuthedUserSucces() {
  return {
    type: LOGOUT_AUTHED_USER_SUCCES,
  };
}

export function oAuthSignInStart(provider) {
  return {
    type: OAUTH_SIGN_IN_START,
    provider,
  };
}

export function oAuthSignInSucces(provider) {
  return {
    type: OAUTH_SIGN_IN_SUCCES,
    provider,
  };
}

export function oAuthSignInError(error) {
  return {
    type: OAUTH_SIGN_IN_ERROR,
    error,
  };
}

export function getAuthedUser() {
  return (dispatch) => {
    dispatch(getAuthedUserRequest());
    return fetch('/api/login')
      .then(checkStatus)
      .then(parseResponse)
      .then((data) => {
        dispatch(getAuthedUserSucces(data));

        LogRocket.identify(data.uuid, {
          name: data.displayName,
        });
      })
      .catch((error) => {
        dispatch(getAuthedUserError(error));
        return Promise.reject(error);
      });
  };
}

export function initAuth() {
  return (dispatch) => {
    if (Cookies.load('logined')) {
      return dispatch(getAuthedUser());
    }
    return null;
  };
}

export function logoutAuthedUser() {
  return (dispatch) => {
    dispatch(logoutAuthedUserSucces());
    Cookies.remove('logined');

    fetch('/api/login', { method: 'delete' });
  };
}

export function listenForOAuthDone(popup) {
  return dispatch => new Promise((resolve, reject) => {
    (function listener() {
      if (popup.closed) {
        return reject(new Error('Authentication was cancelled.'));
      }
      let hostname = '';
      let pathname = '';
      try {
        const location = popup.location;
        hostname = location.hostname;
        pathname = location.pathname;
      } catch (err) {}

      if (hostname === APPLICATION_HOSTNAME && pathname === '/signin') {
        popup.close();
        return dispatch(getAuthedUser())
        .then(() => {
          resolve();
        })
        .catch(error => reject(error));
      }

      return setTimeout(() => listener(), 10);
    }());
  });
}

export function oAuthSignIn(provider) {
  return (dispatch) => {
    dispatch(oAuthSignInStart(provider));
    const popup = openPopup(provider);

    return dispatch(listenForOAuthDone(popup))
      .then(() => {
        Cookies.save('logined', true);
        dispatch(oAuthSignInSucces(provider));
      })
      .catch(error => dispatch(oAuthSignInError(error)));
  };
}


export function setupSession(token) { // to delete
  return () => {
    Cookies.save('SESSION', token);
  };
}

export function removeSession() { // to delete
  return () => {
    Cookies.remove('SESSION');
  };
}
