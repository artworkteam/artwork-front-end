import { fetch, checkStatus, parseResponse } from '../utils';
import { pinWorkById } from './WorksActions';
import {
  GET_SUGGESTIONS_REQUEST,
  GET_SUGGESTIONS_SUCCES,
  GET_SUGGESTIONS_ERROR,
  ADD_SUGGESTION_REQUEST,
  ADD_SUGGESTION_SUCCES,
  ADD_SUGGESTION_ERROR,
  MARK_SUGGESTION_READED_REQUEST,
  MARK_SUGGESTION_READED_SUCCES,
  MARK_SUGGESTION_READED_ERROR,
  SELECT_SUGGESTION,
  SHOW_PREVIEW,
  HIDE_PREVIEW,
  RESET_SELECTED,
  INITIAL_SUGGESTIONS_START,
  INITIAL_SUGGESTIONS_END,
} from '../constants/Suggestions';

export function getSuggestionsRequest() {
  return {
    type: GET_SUGGESTIONS_REQUEST,
  };
}

export function getSuggestionsSucces(suggestions) {
  return {
    type: GET_SUGGESTIONS_SUCCES,
    suggestions,
  };
}

export function getSuggestionsError(error) {
  return {
    type: GET_SUGGESTIONS_ERROR,
    error,
  };
}

export function addSuggestionRequest() {
  return {
    type: ADD_SUGGESTION_REQUEST,
  };
}

export function addSuggestionSucces(suggestion) {
  return {
    type: ADD_SUGGESTION_SUCCES,
    suggestion,
  };
}

export function addSuggestionError(error) {
  return {
    type: ADD_SUGGESTION_ERROR,
    error,
  };
}

export function markSuggestionReadedRequest(suggestionId) {
  return {
    type: MARK_SUGGESTION_READED_REQUEST,
    suggestionId,
  };
}

export function markSuggestionReadedSucces(suggestion) {
  return {
    type: MARK_SUGGESTION_READED_SUCCES,
    suggestion,
  };
}

export function markSuggestionReadedError(error) {
  return {
    type: MARK_SUGGESTION_READED_ERROR,
    error,
  };
}

export function selectSuggestion(suggestionId) {
  return {
    type: SELECT_SUGGESTION,
    suggestionId,
  };
}

export function showPreview(x, y) {
  return {
    type: SHOW_PREVIEW,
    coords: { x, y },
  };
}

export function hidePreview() {
  return {
    type: HIDE_PREVIEW,
  };
}

export function resetSelected() {
  return {
    type: RESET_SELECTED,
  };
}


export function getSuggestions(workId) {
  return (dispatch) => {
    dispatch(getSuggestionsRequest());

    return fetch(`/api/works/${workId}/suggestion-groups`)
      .then(checkStatus)
      .then(parseResponse)
      .then(data => dispatch(getSuggestionsSucces(data._embedded.suggestionGroupResourceList)))
      .catch(error => dispatch(getSuggestionsError(error)));
  };
}

export function addSuggestion(workId, pointX, pointY, text) {
  return (dispatch) => {
    dispatch(addSuggestionRequest());
    return fetch(`/api/works/${workId}/suggestions`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        pointX,
        pointY,
        text,
      }),
    })
      .then(checkStatus)
      .then(parseResponse)
      .then(data => dispatch(addSuggestionSucces(data)))
      .catch(error => dispatch(addSuggestionError(error)));
  };
}

export function markSuggestionReaded(workId, suggestionId) {
  return (dispatch) => {
    dispatch(markSuggestionReadedRequest(suggestionId));
    return fetch(`/api/works/${workId}/suggestions/${suggestionId}?read=true`, {
      method: 'PUT',
    })
      .then(() => dispatch(getSuggestions(workId)));
  };
}

export function initSuggestions(workId) {
  return (dispatch, getState) => {
    dispatch({ type: INITIAL_SUGGESTIONS_START });

    const { entities } = getState();
    const { works } = entities;
    const work = works[workId];
    if (!work) {
      dispatch(pinWorkById(workId))
        .then(() => dispatch(getSuggestions(workId)))
        .then(() => dispatch({ type: INITIAL_SUGGESTIONS_END }));
    } else {
      dispatch(getSuggestions(workId))
        .then(() => dispatch({ type: INITIAL_SUGGESTIONS_END }));
    }
  };
}
