import {
  SHOW_ERROR_ALERT,
  SHOW_SUCCESS_ALERT,
  CLEAR_ALERT,
} from '../constants/Alerts';

export const showErrorAlert = message => ({
  type: SHOW_ERROR_ALERT,
  payload: message,
});

export const showSuccessAlert = message => ({
  type: SHOW_SUCCESS_ALERT,
  payload: message,
});

export const clearAlert = () => ({
  type: CLEAR_ALERT,
});
