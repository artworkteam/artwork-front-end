import { browserHistory } from 'react-router';

export function routeChanged(url) {
  return {
    type: 'ROUTE_CHANGED',
    url,
  };
}

export function push(url) {
  return () => {
    browserHistory.push(url);
  };
}

export function goBack() {
  return () => {
    browserHistory.goBack();
  };
}
