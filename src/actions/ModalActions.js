import { SHOW_MODAL, HIDE_MODAL } from '../constants/Modal';

export function showModal(modalType) {
  return {
    type: SHOW_MODAL,
    modalType,
  };
}

export function hideModal() {
  return {
    type: HIDE_MODAL,
  };
}
