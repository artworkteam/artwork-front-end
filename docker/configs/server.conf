
upstream backend {
    include     /etc/nginx/server-upstream.conf;
    keepalive 32;
}

server {
    listen      80;
    return      301 https://$host$request_uri;
}

server {
    include     /etc/nginx/ssl.conf;
    include     /etc/nginx/server-name.conf;

    client_max_body_size 6m;
    keepalive_timeout 70;

    root /artwork-front/public;

    location / {
        index index.html;
    }

    location /works {
        rewrite ^ /index.html break;
    }

    location /notifications {
        rewrite ^ /index.html break;
    }

    location /api/notifications/stream {
        proxy_buffering off;
        proxy_cache off;
        proxy_set_header Connection '';
        proxy_http_version 1.1;
        chunked_transfer_encoding off;

        proxy_pass https://backend;
    }

    location /api {
        add_header Cache-Control no-store;
        add_header Pragma no-cache;
        add_header X-Frame-Options SAMEORIGIN;

        proxy_set_header Connection '';
        proxy_http_version 1.1;

        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header   X-Forwarded-Proto $scheme;
        proxy_set_header Host $http_host;
        proxy_redirect off;
        proxy_connect_timeout 120s;
        proxy_read_timeout 240s;

        proxy_pass https://backend;
    }

    location /management/ {
        add_header Cache-Control no-store;
        add_header Pragma no-cache;
        add_header X-Frame-Options SAMEORIGIN;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header   X-Forwarded-Proto $scheme;
        proxy_set_header Host $http_host;
        proxy_redirect off;
        proxy_connect_timeout 120s;
        proxy_read_timeout 240s;

        proxy_pass https://backend;
    }

    include     /etc/nginx/swagger.conf;

}
