const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const path = require('path');

module.exports = {
  context: path.join(__dirname),
  entry: {
    app: [
      'react-hot-loader/patch',
      './src/index.js',
    ],
    vendor: ['react', 'react-dom'],
  },
  output: {
    filename: '[name].bundle.js',
    path: path.join(__dirname, './dist/'),
  },
  devtool: 'eval',
  module: {
    rules: [
      {
        test: /\.(js)$/,
        exclude: /node_modules/,
        use: ['babel-loader'],
      },
      {
        test: /\.css$/,
        exclude: /node_modules/,
        /*loader: ExtractTextPlugin.extract({
          fallbackLoader: "style-loader",
          loader: "css-loader?modules&importLoaders=1!postcss-loader",
        }),*/
        loader: "style-loader!css-loader?modules&importLoaders=1&localIdentName=[name]__[local]!postcss-loader",
      },
      {
        test: /\.svg$/,
        loader: "url-loader?limit=10000&mimetype=image/svg+xml",
      },
      {
        test: /\.(png|woff|woff2|eot|ttf)$/,
        loader: 'file-loader?name=fonts/[name].[ext]',
      },

    ],
  },
  plugins: [
    new webpack.optimize.CommonsChunkPlugin({
      name: 'vendor',
      minChunks: Infinity,
      filename: '[name].bundle.js',
    }),
    new HtmlWebpackPlugin({
      template: './src/index.html',
      filename: 'index.html',
      inject: 'body',
    }),
    //new ExtractTextPlugin('bundle.css'),
  ],
  devServer: {
    contentBase: './src',
    port: 7000,
    historyApiFallback: true,
    proxy: [
      {
        context: ['/'],
        target: 'https://beta.artwork.pro/',
        secure: false,
      }
    ],
  },
};
