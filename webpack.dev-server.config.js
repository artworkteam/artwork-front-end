const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const devConfig = require('./webpack.dev.config');

devConfig.plugins = [
  new HtmlWebpackPlugin({
    template: './static/index.html',
    filename: 'index.html',
    inject: 'body',
  }),
  new CopyWebpackPlugin([
    {
      context: 'static',
      from: '**',
      ignore: ['index.html'],
    },
  ]),
  new webpack.DefinePlugin({
    DEV_SERVER: JSON.stringify(true),
  }),
];

devConfig.devServer = {
  contentBase: './src',
  port: 7000,
  historyApiFallback: true,
  proxy: [
    {
      context: ['/'],
      target: 'https://api.artwork.pro',
      secure: false,
      bypass: function(req, res, proxyOptions) {
        if (req.headers.accept.indexOf('html') !== -1) {
          return '/index.html';
        }
      },
    },
  ],
};

module.exports = devConfig;
