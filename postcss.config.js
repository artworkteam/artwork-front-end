const mixinsFile = require('./src/styles/mixins.js');

module.exports = {
  plugins: [
    require('postcss-use')({ modules: 'lost' }),
    require('precss')({
      import: { disable: true },
      media: {
        extensions: {
          '--extra-small': '(max-width: 575px)',
          '--small': '(max-width: 767px)',
          '--medium': '(max-width: 991px)',
          '--large': '(max-width: 1199px)',
        },
      },
      mixins: {
        mixins: mixinsFile,
      },
    }),
    require('autoprefixer'),
    require('postcss-flexbugs-fixes'),
    require('postcss-image-set-polyfill'),
  ],
};
