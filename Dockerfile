FROM nginx

ARG PROJECT

ADD ./dist          /artwork-front/public


RUN ls /artwork-front/public -la

ADD docker/configs/artwork.key          /etc/nginx/artwork.key
ADD docker/configs/artwork.pem          /etc/nginx/artwork.pem
ADD docker/configs/mime.types           /etc/nginx/mime.types
ADD docker/configs/server.conf          /etc/nginx/conf.d/default.conf
ADD docker/configs/server-name.conf     /etc/nginx/server-name.conf
ADD docker/configs/proxy.conf           /etc/nginx/proxy.conf
ADD docker/configs/server-upstream.conf /etc/nginx/server-upstream.conf
ADD docker/configs/ssl.conf             /etc/nginx/ssl.conf
ADD docker/configs/swagger.conf         /etc/nginx/swagger.conf

RUN ls /etc/nginx -la
RUN ls /etc/nginx/conf.d -la

RUN cat /etc/nginx/conf.d/default.conf
RUN cat /etc/nginx/nginx.conf


EXPOSE 80
EXPOSE 443

#sudo docker run -p 443:443 -p 80:80 -v /home/ec2-user/log:/var/log/nginx/ -d kepkap/artwork-front-beta
