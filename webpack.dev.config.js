const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const path = require('path');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const eslintConfig = require('./.eslintrc');

module.exports = {
  context: path.join(__dirname),
  entry: {
    app: [
      'react-hot-loader/patch',
      './src/index.js',
    ],
  },
  output: {
    filename: '[name].bundle.js',
    path: path.join(__dirname, './dist/'),
    publicPath: '/',
  },
  devtool: 'source-map',
  module: {
    rules: [
      /*{
        test: /\.js$/,
        loader: 'eslint-loader',
        exclude: /node_modules/,
        enforce: 'pre',
        query: eslintConfig,
      },*/
      {
        test: /\.(js)$/,
        exclude: /node_modules/,
        use: ['babel-loader'],
      },
      {
        test: /\.(jpe?g|png|ttf|eot|svg|woff(2)?)(\?[a-z0-9=&.]+)?$/,
        use: [
          {
            loader: 'base64-inline-loader',
            options: {
              limit: '10000',
              name: 'name=[path][name].[ext]',
            },
          },
        ],
      },
      {
        test: /\.css$/,
        exclude: /node_modules/,
        // use: ['css-loader', 'postcss-loader'],
        use: [
          {
            loader: 'style-loader',
          },
          {
            loader: 'css-loader',
            options: { modules: true, importLoaders: '1', localIdentName: '[name]__[local]' },
          },
          {
            loader: 'postcss-loader',
          },
        ],
      },
    ],
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: './static/index.html',
      filename: 'index.html',
      inject: 'body',
    }),
    new CopyWebpackPlugin([
      {
        context: 'static',
        from: '**',
        ignore: ['index.html'],
      },
    ]),
    new webpack.DefinePlugin({
      DEV_SERVER: JSON.stringify(false),
    }),
  ],
};
