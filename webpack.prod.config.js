const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const path = require('path');

module.exports = {
  context: path.join(__dirname),
  entry: {
    app: [
      './src/index.js',
    ],
    vendor: [
      'react',
      'react-cookie',
      'react-dom',
      'react-modal',
      'react-redux',
      'react-router',
      'redux',
      'redux-form',
      'redux-thunk',
      'normalizr',
    ],
  },
  output: {
    filename: '[name].[chunkhash].bundle.js',
    path: path.join(__dirname, './dist/'),
    publicPath: '/',
  },
  module: {
    rules: [
      {
        test: /\.(js)$/,
        exclude: /node_modules/,
        use: ['babel-loader'],
      },
      {
        test: /\.css$/,
        exclude: /node_modules/,
        use: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: [
            {
              loader: 'css-loader',
              options: { modules: true, importLoaders: '1' },
            },
            {
              loader: 'postcss-loader',
            },
          ],
        }),
      },
      {
        test: /\.(jpe?g|png|ttf|eot|svg|woff(2)?)(\?[a-z0-9=&.]+)?$/,
        use: [
          {
            loader: 'base64-inline-loader',
            options: {
              limit: '60000',
              name: 'name=[path][name].[ext]',
            },
          },
        ],
      },
    ],
  },
  plugins: [
    new webpack.optimize.CommonsChunkPlugin({
      name: 'vendor',
      minChunks: Infinity,
      filename: '[name].[chunkhash].bundle.js',
    }),
    new HtmlWebpackPlugin({
      template: './static/index.html',
      filename: 'index.html',
      inject: 'body',
    }),
    new ExtractTextPlugin('bundle.css'),
    new CopyWebpackPlugin([
      {
        context: 'static',
        from: '**/*',
        ignore: ['index.html'],
      },
    ]),
    new webpack.DefinePlugin({
      DEV_SERVER: JSON.stringify(false),
    }),
  ],
};
